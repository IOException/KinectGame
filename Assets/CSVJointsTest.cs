﻿using UnityEngine;
using System.Collections;
using System.IO;
// Only saves one filename currently. Close the file that you are saving to before running the script, or else you will get a file sharing error.
public class CSVJointsTest : MonoBehaviour
{
    [Tooltip("The Kinect joint we want to track.")]
    public KinectInterop.JointType RHand = KinectInterop.JointType.HandRight;
    public KinectInterop.JointType RElbow = KinectInterop.JointType.ElbowRight;//set up script for these too
    public KinectInterop.JointType RShoulder = KinectInterop.JointType.ShoulderRight;//ditto


    [Tooltip("Current joint position in Kinect coordinates (meters).")]
    public Vector3 RHandPosition;
    public Vector3 RElbowPosition;
    public Vector3 RShoulderPosition;



    [Tooltip("Whether we save the joint data to a CSV file or not.")]
    public bool isSaving = true;

    [Tooltip("Path to the CSV file, we want to save the joint data to.")]
    public string saveFilePath = "JointsTest.csv";

    [Tooltip("How many seconds to save data to the CSV file, or 0 to save non-stop.")]
    public float secondsToSave = 0f;


    // start time of data saving to csv file
    private float saveStartTime = -1f;


    void Start()
    {

        if (isSaving && File.Exists(saveFilePath))
        {
            File.Delete(saveFilePath);

        }
    }


    void Update()
    {
     
        if (isSaving)
        {
            // create the file, if needed
            if (!File.Exists(saveFilePath))
            {
                using (StreamWriter writer = File.CreateText(saveFilePath))
                {
                    Debug.Log("we made it here");
                    // csv file header
                    string sLine = "time,RHand,pos_x,pos_y,poz_z,RElbow,pos_x,pos_y,poz_z,RShoulder,pos_x,pos_y,poz_z";
                    writer.WriteLine(sLine);
                }
            }


            // check the start time
            if (saveStartTime < 0f)
            {
                saveStartTime = Time.time;
            }
        }

        // get the joint position
        KinectManager manager = KinectManager.Instance;

        if (manager && manager.IsInitialized())
        {
            if (manager.IsUserDetected())
            {
                long userId = manager.GetPrimaryUserID();

                if (manager.IsJointTracked(userId, (int)RHand) &&
                    manager.IsJointTracked(userId, (int)RElbow) &&
                    manager.IsJointTracked(userId, (int)RShoulder))

                {
                    // output the joint position for easy tracking
                    Vector3 RHandPos = manager.GetJointPosition(userId, (int)RHand);
                    RHandPosition = RHandPos;

                    Vector3 RElbowPos = manager.GetJointPosition(userId, (int)RElbow);
                    RElbowPosition = RElbowPos;

                    Vector3 RShoulderPos = manager.GetJointPosition(userId, (int)RShoulder);
                    RShoulderPosition = RShoulderPos;

                    if (isSaving)
                    {
                        if ((secondsToSave == 0f) || ((Time.time - saveStartTime) <= secondsToSave))
                        {
                            using (StreamWriter writer = File.AppendText(saveFilePath))
                            {
                                string sLine = string.Format("{0:F3},{1},{2:F3},{3:F3},{4:F3},{5},ing {6:F3},{7:F3},{8:F3},{9},{10:F3}, {11:F3} ,{12:F3}",
                               Time.time, ((KinectInterop.JointType)RHand).ToString(), RHandPos.x, RHandPos.y, RHandPos.z, ((KinectInterop.JointType)RElbow).ToString(), RElbowPos.x, RElbowPos.y, RElbowPos.z, ((KinectInterop.JointType)RShoulder).ToString(), RShoulderPos.x, RShoulderPos.y, RShoulderPos.z);
                                writer.WriteLine(sLine);
                            }
                        }
                    }
                }
            }
        }

    }

}
