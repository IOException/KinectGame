﻿using UnityEngine;
using System.Collections;

public class DatCollectionBoundary : MonoBehaviour {
    public bool RunLegs=false;

    float startTime;
	// Use this for initialization
	void Start () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (CompareTag("DataCollectionBoundary"))
        {

            startTime = Time.time;
            while (Time.time - startTime < 0.4f)
            { RunLegs = true; }


            RunLegs = false;
        }

    }
	// Update is called once per frame
	void Update () {
	
	}
}