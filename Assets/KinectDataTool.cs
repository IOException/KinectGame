﻿/*
Multithreading Kinect Data Recording Tool

User Manual

1. User API
    The only method you need to call is saveData which is in the KinectDataTool class. 
    Parameter explain: 
        a. dataSetIndex, the index number you want to use, you can set it in the scene inspector. 
        b. rate, time between two data record, set it to 0 to save every frame. Ex: 0.1 for 10 times per second.
        c. timeLength, length of record, set it to 0 for nonstop.
        d. fileName, name of the csv file you want to use to save data. ex: Test1.csv, file will be saved in
           project path.

    There should be only one KinectDataTool per scene and you call it everytime you need to save data, there 
    are many ways to do it, ex: using findGameObject and GetComponent. Check DataToolTester.cs for an example.

2. Extension
    a. Data type. This tool only supporting angle mersuring data for now and it will support velocity measuring
       soon.
    b. It is easy to add more data type if needed, you can edit DataSet class for more option and edit collectData
       method for more function
*/

using UnityEngine;
using System.Collections;
using System.IO;

public class KinectDataTool : MonoBehaviour
{
    [Tooltip("The Kinect joint need to use, it will use core joints for velocity records")]
    public DataSet[] DataSets;

    //The only method you need to call!
    public void saveData(int dataSetIndex, float rate, float timeLength, string fileName)
    {
        SaveData saveData = gameObject.AddComponent<SaveData>();
        saveData.receiveData(DataSets[dataSetIndex], rate, timeLength, fileName);
    }
    
}

public class SaveData : MonoBehaviour
{
    KinectInterop.JointType joint1;
    KinectInterop.JointType coreJoint;
    KinectInterop.JointType joint2;

    Vector3 joint1Pos;
    Vector3 coreJointPos;
    Vector3 joint2Pos;

    KinectManager manager = KinectManager.Instance;

    float endTime;
    float saveRate;
    string saveFileName;
    public float data = 0;
    float previousData = 0;

    //0 for angle, 1 for velocity
    int type;


    public void receiveData(DataSet dataSet, float rate, float timeLength, string fileName)
    {
        coreJoint = dataSet.CoreJoint;
        if (dataSet.dataType == DataSet.DataType.angle)
        {
            type = 0;
            joint1 = dataSet.Joint1;
            joint2 = dataSet.Joint2;
        }
        else
        {
            type = 1;
        }
        endTime = Time.time + timeLength;

        if (timeLength == 0)
        {
            endTime = 0;
        }
        saveRate = rate;
        saveFileName = fileName;
        StartCoroutine(collectData());
    }


    IEnumerator collectData()
    {
        while (Time.time < endTime&&endTime!=0)
        {
            //save data
            if (!File.Exists(saveFileName))
            {
                using (StreamWriter writer = File.CreateText(saveFileName))
                {
                    string sLine;
                    // csv file header;
                    if (type == 0)
                    {
                        sLine = "time, Joint1_x, Joint1_y, Joint1_z, Core_x, Core_y, Core_z, Joint2_x, Joint2_y, Joint2_z, Angle, Angle Change";
                    }
                    else
                    {
                        sLine = "Core_x, Core_y, Core_z, velocity";
                    }
                    writer.WriteLine(sLine);
                }
            }

            if (manager && manager.IsInitialized())
            {
                if (manager.IsUserDetected())
                {
                    long userId = manager.GetPrimaryUserID();
                    //for angle
                    if (type == 0)
                    {
                        if (manager.IsJointTracked(userId, (int)joint1) &&
                        manager.IsJointTracked(userId, (int)coreJoint) &&
                        manager.IsJointTracked(userId, (int)joint2))
                        {
                            joint1Pos = manager.GetJointPosition(userId, (int)joint1);
                            coreJointPos = manager.GetJointPosition(userId, (int)coreJoint);
                            joint2Pos = manager.GetJointPosition(userId, (int)joint2);

                            Vector3 vector1 = joint1Pos - coreJointPos;
                            Vector3 vector2 = joint2Pos - coreJointPos;

                            data = Vector3.Angle(vector1, vector2);
                            float changeInData = (data - previousData) / Time.deltaTime;
                            previousData = data;

                            using (StreamWriter writer = File.AppendText(saveFileName))
                            {
                                string sLine = string.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",

                                Time.time, joint1Pos.x, joint1Pos.y, joint1Pos.z, coreJointPos.x, coreJointPos.y, coreJointPos.z, joint2Pos.x, joint2Pos.y, joint2Pos.z, data, changeInData);
                                writer.WriteLine(sLine);
                            }
                        }
                    }
                }
            }


            yield return new WaitForSeconds(saveRate);
        }
        Destroy(this);
    }
}

[System.Serializable]
public class DataSet : System.Object
{
    public KinectInterop.JointType Joint1;
    public KinectInterop.JointType CoreJoint;
    public KinectInterop.JointType Joint2;
    public DataType dataType;
    public enum DataType
    {
        angle,
        velocity
    }   
}