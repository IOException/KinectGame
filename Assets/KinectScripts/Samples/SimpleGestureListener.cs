using UnityEngine;
//using Windows.Kinect;
using System.Collections;
using System;


public class SimpleGestureListener : MonoBehaviour, KinectGestures.GestureListenerInterface
{
	[Tooltip("GUI-Text to display gesture-listener messages and gesture information.")]
	public GUIText GestureInfo;
	
	// private bool to track if progress message has been displayed
	private bool progressDisplayed;
	private float progressGestureTime;
	
	public bool start, completed, squat, leanl, leanr, kickl, kickr = false;
	public int squatCount = 3;
	
	public void UserDetected(long userId, int userIndex)
	{
		// as an example - detect these user specific gestures
		KinectManager manager = KinectManager.Instance;
		//manager.DetectGesture(userId, KinectGestures.Gestures.Jump);
		manager.DetectGesture(userId, KinectGestures.Gestures.Squat);
		manager.DetectGesture(userId, KinectGestures.Gestures.LeanLeft);
		manager.DetectGesture(userId, KinectGestures.Gestures.LeanRight);
		manager.DetectGesture(userId, KinectGestures.Gestures.KickLeft);
		manager.DetectGesture(userId, KinectGestures.Gestures.KickRight);
		manager.DetectGesture(userId, KinectGestures.Gestures.RaiseLeftHand);
		manager.DetectGesture(userId, KinectGestures.Gestures.RaiseRightHand);
		
		if(GestureInfo != null)
		{
			GestureInfo.GetComponent<GUIText>().text = "To start warming up, raise a hand.";
		}
	}
	
	public void UserLost(long userId, int userIndex)
	{
		if(GestureInfo != null)
		{
			GestureInfo.GetComponent<GUIText>().text = string.Empty;
		}
	}
	
	public void GestureInProgress(long userId, int userIndex, KinectGestures.Gestures gesture, 
	                              float progress, KinectInterop.JointType joint, Vector3 screenPos)
	{
		/*if((gesture == KinectGestures.Gestures.ZoomOut || gesture == KinectGestures.Gestures.ZoomIn) && progress > 0.5f)
		{
			string sGestureText = string.Format ("{0} - {1:F0}%", gesture, screenPos.z * 100f);
			if(GestureInfo != null)
			{
				GestureInfo.GetComponent<GUIText>().text = sGestureText;
			}

			progressDisplayed = true;
			progressGestureTime = Time.realtimeSinceStartup;
		}
		else if((gesture == KinectGestures.Gestures.Wheel || gesture == KinectGestures.Gestures.LeanLeft || 
		         gesture == KinectGestures.Gestures.LeanRight) && progress > 0.5f)
		{
			string sGestureText = string.Format ("{0} - {1:F0} degrees", gesture, screenPos.z);
			if(GestureInfo != null)
			{
				GestureInfo.GetComponent<GUIText>().text = sGestureText;
			}

			//Debug.Log(sGestureText);
			progressDisplayed = true;
			progressGestureTime = Time.realtimeSinceStartup;
		}*/
		if (gesture == KinectGestures.Gestures.LeanRight && progress > 0.5f && leanr == true) {
			string sGestureText = string.Format ("Lean Right - {0:F0} degrees", screenPos.z);
			
			if(screenPos.z >= 25)
			{
				leanr = false;
				leanl = true;
				sGestureText = "Lastly, lean left 20 degrees...";
			}
			
			if(GestureInfo != null)
			{
				GestureInfo.GetComponent<GUIText>().text = sGestureText;
			}

			progressDisplayed = true;
			progressGestureTime = Time.realtimeSinceStartup;
		}
		else if (gesture == KinectGestures.Gestures.LeanLeft && progress > 0.5f && leanl == true) {
			float z = screenPos.z;
			string sGestureText = string.Format ("Lean Left - {0:F0} degrees", z);
			
			if(z >= 25)
			{
				leanl = false;
				completed = true;
			}
			
			if(GestureInfo != null)
			{
				GestureInfo.GetComponent<GUIText>().text = sGestureText;
			}
			
			progressDisplayed = true;
			progressGestureTime = Time.realtimeSinceStartup;
		}		
	}
	
	public bool GestureCompleted(long userId, int userIndex, KinectGestures.Gestures gesture, 
	                             KinectInterop.JointType joint, Vector3 screenPos)
	{
		string sGestureText = string.Empty;
		
		if(progressDisplayed)
			return true;
		
		if ((gesture == KinectGestures.Gestures.RaiseLeftHand ||
		     gesture == KinectGestures.Gestures.RaiseRightHand) &&
		    start == false) 
		{
			start = true;
			kickr = true;
			
			sGestureText = "Ok, let's start! Give me a right kick!";
		}
		else if (gesture == KinectGestures.Gestures.KickRight && kickr == true) {
			kickr = false;
			kickl = true;
			sGestureText = "Great! Now try a left kick...";
		}
		else if (gesture == KinectGestures.Gestures.KickLeft && kickl == true) {
			kickl = false;
			squat = true;
			sGestureText = string.Format("Alright! You're doing a great job!\nNow squat {0} times...", squatCount);
		}
		else if (gesture == KinectGestures.Gestures.Squat && squat == true) {
			if(squatCount > 0)
			{
				squatCount--;
				if(squatCount > 1)
					sGestureText = string.Format("Just {0} to go...", squatCount);
				else
					sGestureText = "Good. One more!";
			}
			else
			{
				squat = false;
				leanr = true;
				sGestureText = "Wow! Impressive... We're almost done.\nLean 25 degress to the right.";
			}
		}
		else if (!leanr && leanl)
			sGestureText = sGestureText = "Lastly, lean left 25 degrees...";
		
		if(GestureInfo != null)
		{
			GestureInfo.GetComponent<GUIText>().text = sGestureText;
		}
		
		return true;
	}
	
	public bool GestureCancelled(long userId, int userIndex, KinectGestures.Gestures gesture, 
	                             KinectInterop.JointType joint)
	{
		if(progressDisplayed)
		{
			progressDisplayed = false;
			
			if(GestureInfo != null)
			{
				GestureInfo.GetComponent<GUIText>().text = String.Empty;
			}
		}
		
		return true;
	}
	
	public void Update()
	{
		if(progressDisplayed && ((Time.realtimeSinceStartup - progressGestureTime) > 2f))
		{
			progressDisplayed = false;
			
			if(GestureInfo != null)
			{
				GestureInfo.GetComponent<GUIText>().text = String.Empty;
			}
			
			Debug.Log("Forced progress to end.");
		}

		//if(completed)
		//	GestureInfo.GetComponent<GUIText>().text = "Awesome! You have completed your warm up.";
	}
	
}
