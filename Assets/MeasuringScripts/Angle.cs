﻿using System.Collections;
using System.Xml;
using System.Xml.Serialization;
public class Angle
{
    [XmlAttribute("Angle")]
    public double number;
}
