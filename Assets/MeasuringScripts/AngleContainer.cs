﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
[XmlRoot("AngleCollection")]
public class AngleContainer
{
    [XmlArray("Angles")]
    [XmlArrayItem("Angle")]
    public List<float> Angles = new List <float>();
    public float max;
    public float avg;
    public List<float> ChangeInAngles = new List<float>();
    public List<float> Time = new List<float>();
    public float maxChange;
        public float avgChange;
    public List<float> PunchVelocity=new List<float>();
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(AngleContainer));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
    public static AngleContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(AngleContainer));
        using (var stream = new FileStream(path, FileMode.Open))
     
        {
            return serializer.Deserialize(stream) as AngleContainer;
        }     
}
}
