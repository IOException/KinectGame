﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
[XmlRoot("DataCollection")]
public class DataContainer
{
    [XmlArray("Seconds")]
    [XmlArrayItem("TimeLapse")]
    public List<TimeLapse>Seconds = new List<TimeLapse>();

    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(DataContainer));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
}



/* using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

[XmlRoot("BMEData")]
public class BiomechanicalContainer
{


[XmlArray("BiomechanicalDataClass")]

[XmlArrayItem("Angles")]
public List <BiomechanicalData>RightArmAngle = new List<BiomechanicalData>();

public void save(string path)
{
   var serializer = new XmlSerializer(typeof(BiomechanicalContainer));
   using (var stream = new FileStream(path, FileMode.Create))
   {
       serializer.Serialize(stream, this);
   }



}
}*/
