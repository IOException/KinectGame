﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("ChangeInAngleCollection")]

public class ChangeInAnglesContainer
{
   
    [XmlArray("ChangeInAngles")]
    [XmlArrayItem("ChangeInAngle")]
    public List<float> ChangeInAngles = new List<float>();
    public float maxChange;
    public float avgChange;
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(ChangeInAnglesContainer));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
    public static ChangeInAnglesContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(ChangeInAnglesContainer));
        using (var stream = new FileStream(path, FileMode.Open))

        {
            return serializer.Deserialize(stream) as ChangeInAnglesContainer;
        }
    }
}