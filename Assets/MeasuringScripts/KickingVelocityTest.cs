﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
public class KickingVelocityTest : MonoBehaviour
{
    bool iskicking;
    List<float> Velocities = new List<float>();

    VelocityContainer Kcontainer = new VelocityContainer();

    // The Punch Velocity Test should be attached to the gameobject that you want the velocity of, due to it's use of the Rigidbody


    public GameObject Rfoot;

    Rigidbody RB;


    void FixedUpdate()
    {

        VelocityCalculate(Rfoot);

    }
    void OnApplicationQuit()
    {
        float max = Velocities.Max();
        float average = Velocities.Average();
        Debug.Log(max + " " + average);
        Kcontainer.avg = average;
        Kcontainer.max = max;
        Kcontainer.Save(Path.Combine(Application.dataPath, "KickSpeeds.xml"));
    }
    void VelocityCalculate(GameObject Joint1)
    {
        if (RB.velocity.magnitude > 0.5f)
        { iskicking = true; }

        else {return; }

        while (iskicking && gameObject.GetComponent<CombatPlayerController>().RightArmReady == true)
        {
            float temp = RB.velocity.magnitude;
            Kcontainer.Velocities.Add(temp);
            Velocities.Add(temp);
        }
    }


}
