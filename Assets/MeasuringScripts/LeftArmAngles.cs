﻿
//Assign LeftArmAngles to the LeftHandRB object. Otherwise, the scipt will not be able to access the appropriate rigidbody and an error will occur.




using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;


public class LeftArmAngles : MonoBehaviour

{
    List<float> AnglesList = new List<float>();
    List<float> ChangeinAngles = new List<float>();
    List<float> time = new List<float>();
    
    List<float> punchVelocity = new List<float>(); 

    AngleContainer LHcontainer = new AngleContainer();

    public float rate;
    public float waitBeforeMeasure;
    public GameObject LShoulder;
    public GameObject LElbow;
    public GameObject LHand;
    public bool LHpunching=false;

    public float punchSpeed;
    Vector3 oldPosition;
    Vector3 newPosition;
    float velocity;




    // Update is called once per frame
    void FixedUpdate()
    {
        newPosition = transform.position;
        var media = (newPosition - oldPosition);
        velocity = media.magnitude / Time.deltaTime;
        oldPosition = newPosition;
        newPosition = transform.position;

        if (velocity > punchSpeed)
        { LHpunching = true; 

        Debug.Log("IsPunching = true;");
    }   
        else{ LHpunching = false; }
   }
    
    //Use this for initialization
    void Start() {
        // trigger to collect data whenever we need it. When do we need it?

        // filtering possibilities for left arm angles: 
        //1) when angle change is large enough=> Indicates punching. 
        ///2) when rb.velocity is fast enough that's a punch=>  issues: The 
        //3) Ala Rob) Acceleration. Use a for loop and determine the acceleration in the hand. Once it hits a threshold, that's a punch. 
        //=> potential issues: for loop implemented packs unweildy calculations into every frame, potential to screw up the time of frames0
        // benefits: more accurate measure of when punch starts and stops.

        //Possible large issues: Punching forward is one of the motions with a high chance for error on the kinect.
        //Plan for implementation: start simple, if simple isn't good enough, go more complex.


        // potential sources of error: glitches. How do we filter those out? ignore motions faster than a certain range. check position
        //while (LHpunching){} 
        oldPosition = LHand.transform.position;


        
        //InvokeRepeating allows control of frequency, but is less accurate than fixed update. If we use a small enough frequency, this shouldn't be an issue.
        InvokeRepeating("anglecount", waitBeforeMeasure,rate);



    }

    void anglecount()
 {
//while (LHpunching)
  //      {
            float result = gameObject.GetComponent<VectorCalculate>().AngleCalculation(LHand, LElbow, LShoulder);
            LHcontainer.Angles.Add(result);
            AnglesList.Add(result);
            float seconds = Time.time;
            time.Add(seconds);
        punchVelocity.Add(velocity);
        LHcontainer.PunchVelocity.Add(velocity);
        Debug.Log(velocity);

        //    }
    }

    void OnApplicationQuit()
    {
        float max = AnglesList.Max();
        float average = AnglesList.Average();
        //Debug.Log(max + " " + average);
        LHcontainer.avg = average;
        LHcontainer.max = max;  

        for (int i = 0; i < AnglesList.Count - 2; i++)
        {
            float changeinangle = (AnglesList[i + 1] - AnglesList[i]) / rate;
            ChangeinAngles.Add(changeinangle);
            LHcontainer.ChangeInAngles.Add(changeinangle);

      
        }
        float maxChange = ChangeinAngles.Max();
        float avgChange = ChangeinAngles.Average();

        LHcontainer.maxChange = maxChange;
        LHcontainer.avgChange = avgChange;
        LHcontainer.maxChange = maxChange;
        LHcontainer.avgChange = avgChange;
        //Debug.Log(maxChange + " " + avgChange);
        
LHcontainer.Save(Path.Combine(Application.dataPath, "LeftArmAngles.xml"));

    }

}
