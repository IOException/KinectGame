﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;

//Ready for Testing
public class LeftLegAngles : MonoBehaviour {
  List<float> AnglesList = new List<float>();

    AngleContainer LFcontainer = new AngleContainer();


    public GameObject LKnee;
    public GameObject LHip;
    public GameObject LFoot;




    void FixedUpdate()
    {
        kicktrigger();   
    }
    void OnApplicationQuit()
    {
        float max = AnglesList.Max();
        float average = AnglesList.Average();
        Debug.Log(max + " " + average);
        LFcontainer.avg = average;
        LFcontainer.max = max;
        LFcontainer.Save(Path.Combine(Application.dataPath, "LeftLegAngles.xml"));
    }
    void kicktrigger()
    {
        /*    if (gameObject.GetComponent<GetKicked>().iskicking == true)
            {*/
        float result = gameObject.GetComponent<VectorCalculate>().AngleCalculation(LHip, LKnee, LFoot);
            LFcontainer.Angles.Add(result);
            AnglesList.Add(result);
        //}
    }
}
