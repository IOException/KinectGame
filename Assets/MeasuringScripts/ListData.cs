﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
public class ListData : MonoBehaviour
{
    List<float> AnglesList = new List<float>();
    List<float> Seconds = new List<float>();

    AngleContainer RHcontainer = new AngleContainer();
    AngleContainer LHcontainer = new AngleContainer();

    public GameObject RShoulder;
    public GameObject RElbow;
    public GameObject RHand;

    public GameObject LShoulder;
    public GameObject LElbow;
    public GameObject LHand;


    //Use this for initialization
    //void Start () {

    //}

    // Update is called once per frame
    void FixedUpdate()
    {
        float result = gameObject.GetComponent<VectorCalculate>().AngleCalculation(RShoulder, RElbow, RHand);
        RHcontainer.Angles.Add(result);
        AnglesList.Add(result);

        float result2 = gameObject.GetComponent<VectorCalculate>().AngleCalculation(LShoulder, LElbow, LHand);
        RHcontainer.Angles.Add(result2);
        AnglesList.Add(result);


        float temp = Time.time;
        Seconds.Add(temp);


        
    }
void OnApplicationQuit()
    {
        float max = Seconds.Max();
        float average = Seconds.Average();
//Debug.Log(max +" "+ average);
        RHcontainer.avg = average;
        RHcontainer.max = max;
        RHcontainer.Save(Path.Combine(Application.dataPath,"data.xml"));
    }

}
