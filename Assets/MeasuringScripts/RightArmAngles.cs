﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
public class RightArmAngles : MonoBehaviour
{
    Vector3 a;
    Vector3 b;
    float c;
    float d;
    float e;
    float result, lastresult;

    List<float> AnglesList = new List<float>();
   // List<float> ChangeinAngles = new List<float>();
   
    AngleContainer RHcontainer = new AngleContainer();
    ChangeInAnglesContainer RHchange = new ChangeInAnglesContainer();

    public GameObject RShoulder, RElbow, RHand;

  


    //Use this for initialization
    //void Start () {
    
    //}

    // Update is called once per frame
    void FixedUpdate()
    {
        result = gameObject.GetComponent<VectorCalculate>().AngleCalculation(RShoulder, RElbow, RHand);//AngleCalculation(RShoulder, RElbow, RHand); //gameObject.GetComponent<VectorCalculate>().AngleCalculation(RShoulder, RElbow, RHand);
        RHcontainer.Angles.Add(result);
        AnglesList.Add(result);

        /* float changeinangle = 2;
        RHchange.ChangeInAngles.Add (changeinangle);
       ChangeinAngles.Add(changeinangle);
       */  

    }
    void OnApplicationQuit()
    {
        float max = AnglesList.Max();
        float average = AnglesList.Average();
       
        RHcontainer.avg = average;
        RHcontainer.max = max;
        RHcontainer.Save(Path.Combine(Application.dataPath, "RightArmAngles.xml"));
/*
        float maxChange = ChangeinAngles.Max();
        float avgChange = ChangeinAngles.Average();
        Debug.Log(maxChange + " " + avgChange);
        RHchange.maxChange = maxChange;
        RHchange.avgChange = avgChange;
        RHchange.Save(Path.Combine(Application.dataPath, "RightArmChangeInAngles.xml"));
  */  }
    /*
    void RateOfAngularChange()
    {for( int i=0; i<1; i++)
        { lastresult = result; }

        float RateofAngleChange =(result - lastresult) / Time.fixedDeltaTime;
        float lastresult = result;
        return  RateofAngleChange;

    }*/
    public float AngleCalculation(GameObject J, GameObject K, GameObject L)
    {//Upper Arm

        a = -J.transform.position + K.transform.position;

        //Forearm
        b = K.transform.position - L.transform.position;

        //Debug.Log(a);
        // Debug.Log(b);

        //c = a*b
        c = a.x * b.x + a.y * b.y + a.z * b.z;
        //d = ||a||   ... Magnitudes 
        d = Mathf.Sqrt(Mathf.Pow(a.x, 2) + Mathf.Pow(a.y, 2) + Mathf.Pow(a.z, 2));
        //e = ||b||
        e = Mathf.Sqrt(Mathf.Pow(b.x, 2) + Mathf.Pow(b.y, 2) + Mathf.Pow(b.z, 2));

        //       C \ (||a|| ||b||)=Angle
        return result = Mathf.Acos(c / (d * e)) * 180 / (22 / 7);




    }
}

