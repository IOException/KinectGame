﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
public class RigthLegAngles : MonoBehaviour
{
    List<float> AnglesList = new List<float>();

    AngleContainer RFcontainer = new AngleContainer();


    public GameObject RKnee;
    public GameObject RHip;
    public GameObject RFoot;




    void FixedUpdate()
    {
        float result = gameObject.GetComponent<VectorCalculate>().AngleCalculation(RHip, RKnee, RFoot);
        RFcontainer.Angles.Add(result);
        AnglesList.Add(result);





    }
    void OnApplicationQuit()
    {
        float max = AnglesList.Max();
        float average = AnglesList.Average();
        Debug.Log(max + " " + average);
        RFcontainer.avg = average;
        RFcontainer.max = max;
        RFcontainer.Save(Path.Combine(Application.dataPath, "RightLegAngles.xml"));
    }

}