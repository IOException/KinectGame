﻿using UnityEngine;

using System.IO;



public class VectorCalculate : MonoBehaviour
{
    public GameObject joint1;
    public GameObject joint2;
    public GameObject joint3;
    public UnityEngine.UI.Text textUI;



    Vector3 a;
    Vector3 b;
    float c;
    float d;
    float e;

    float result;
    string fileName = "vectorAngle.txt";

    StreamWriter myFile;
    // Use this for initialization
    void Start()
    {
        //Creates Txt File
        myFile = File.CreateText(fileName);
    }

    //AngleCaclulation Function to be called elsewhere
    public float AngleCalculation(GameObject J, GameObject K, GameObject L)
    {//Upper Arm

        a = -J.transform.position + K.transform.position;

        //Forearm
        b = K.transform.position - L.transform.position;

        //Debug.Log(a);
        // Debug.Log(b);

        //c = a*b
        c = a.x * b.x + a.y * b.y + a.z * b.z;
        //d = ||a||   ... Magnitudes 
        d = Mathf.Sqrt(Mathf.Pow(a.x, 2) + Mathf.Pow(a.y, 2) + Mathf.Pow(a.z, 2));
        //e = ||b||
        e = Mathf.Sqrt(Mathf.Pow(b.x, 2) + Mathf.Pow(b.y, 2) + Mathf.Pow(b.z, 2));

        //       C \ (||a|| ||b||)=Angle
        return result = Mathf.Acos(c / (d * e)) * 180 / (22 / 7);




    }// Update is called once per frame



    void FixedUpdate()
    {
        AngleCalculation(joint1, joint2, joint3);

        textUI.text = result.ToString();

        myFile.WriteLine(result.ToString());
    } }

    /*  Debug.Log(result);
        
   
    // Finally our save and load methods for the file itself 
    //important
    
      void OnApplicationQuit()
    {
       myFile.Close();
    }
    
}
*/
