﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
[XmlRoot("AngleCollection")]
public class VelocityContainer {
    [XmlArray("Velocities")]
    [XmlArrayItem("Velocity")]
    public List<float> Velocities = new List<float>();
    public float max;
    public float avg;





    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(VelocityContainer));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
    public static VelocityContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(VelocityContainer));
        using (var stream = new FileStream(path, FileMode.Open))

        {
            return serializer.Deserialize(stream) as VelocityContainer;
        }
    }
}
