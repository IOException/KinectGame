﻿using UnityEngine;
using System.Collections;

public class Box : MonoBehaviour {

    private bool connected;
    private GameObject connectedObject;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (connected == true)
        {
            gameObject.transform.position = connectedObject.transform.position;
        }
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hand")
        {
            Debug.Log("Connected");
            connected = true;
            connectedObject = other.gameObject;
        }

        if (other.tag == "Table")
        {
            connected = false;
        }
    }
}
