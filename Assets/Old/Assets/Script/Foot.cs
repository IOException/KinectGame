﻿using UnityEngine;
using System.Collections;

public class Foot : MonoBehaviour {

    public JumpController controller;
    public Foot otherFoot;
    public bool stay;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Floor" && controller.handPositioned==true)
        {
            controller.point++;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Floor")
        {
            stay = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("Enter");
        if (other.tag == "Floor")
        {
            stay = false;
        }
    }
}
