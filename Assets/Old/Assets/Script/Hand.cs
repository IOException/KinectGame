﻿using UnityEngine;
using System.Collections;

public class Hand : MonoBehaviour
{
 

    private float time1;
    private float time2;

    private float x1;
    private float x2;

    private float speed;
    private GameObject wall;


	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
        if (time2 == Time.frameCount)
        {
            x2 = gameObject.transform.position.x;
            CheckSpeed();
        }
	}



	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Wall") {
            wall = other.gameObject;
            time1 = Time.frameCount;
            x1 = gameObject.transform.position.x;
            time2 = time1 + 1;
            
		}
	}

	void OnTriggerExit (Collider other)
	{
	}

    void CheckSpeed()
    {
        speed = (x2 - x1) / (Time.deltaTime);
        if (speed > -0.5)
        {
            wall.GetComponent<WallStats>().HP -= 0;
        }
        else if (speed < -0.5 && speed > -1)
        {
            wall.GetComponent<WallStats>().HP -= 10;
        }
        else if (speed < -1 && speed > -2)
        {
            wall.GetComponent<WallStats>().HP -= 20;
        }
        else if (speed < -2 && speed > -3)
        {
            wall.GetComponent<WallStats>().HP -= 30;
        }
        else if (speed < -3)
        {
            wall.GetComponent<WallStats>().HP -= 40;
        }
        Debug.Log(speed);
    }
}
