﻿using UnityEngine;
using System.Collections;
using System;

public class JumpController : MonoBehaviour
{

	public GameObject leftHand;
	public GameObject rightHand;
	public GameObject leftElbow;
	public GameObject rightElbow;
	public GameObject leftFoot;
	public GameObject rightFoot;

	public GameObject floor;
	public GUIText score;

	public bool jumging;
	public bool handPositioned;
	public int point;
	// Use this for initialization
	void Start ()
	{
		point = 0;
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		checkHandPosition ();
		score.text = "Score: " + point;
	
	}


	void checkHandPosition ()
	{
		if (leftHand.transform.position.x < leftElbow.transform.position.x && rightHand.transform.position.x > rightElbow.transform.position.x) {
			handPositioned = true;
		} else {
			handPositioned = false;
		}

	}
}
