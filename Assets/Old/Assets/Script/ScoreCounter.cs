﻿using UnityEngine;
using System.Collections;

public class ScoreCounter : MonoBehaviour {

    public GUIText scoreText;
    public float score;

    
    private PlayerStats playerStats;

	// Use this for initialization
	void Start () {
        playerStats = GameObject.FindGameObjectWithTag("Player").gameObject.GetComponent<PlayerStats>();
	
	}
	
	// Update is called once per frame
	void Update () {
        score = playerStats.score;
        scoreText.text = "Score: " + score;	

	}
}
