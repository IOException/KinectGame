﻿using System.Xml;
using System.Xml.Serialization;

public class Test
{

    [XmlAttribute("Time")]
    public double time;

    public double x;
    public double y;
    public double z;
}
