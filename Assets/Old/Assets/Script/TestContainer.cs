﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("TestList")]
public class TestContainer
{

    [XmlArray("Tests"), XmlArrayItem("Test")]
    public List<Test> Tests = new List<Test>();

    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(TestContainer));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
}
