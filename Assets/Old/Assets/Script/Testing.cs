﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Testing : MonoBehaviour {

    public GameObject testingObject;
    bool startTest;
    bool isTesting;
    bool end;

    float testTime;
    float endTime;

    TestContainer container2;

    void Start()
    {
        container2 = new TestContainer();
        container2.Tests = new List<Test>();
    }

    void Update()
    {
        if (startTest&& Time.time >= testTime)
        {
            startTest = false;
            isTesting = true;
            endTime = Time.time + 15;
        }
        if(isTesting)
        {
            Debug.Log("Testing");
            Add();
        }

        if (isTesting&&Time.time>=endTime)
        {
            Debug.Log("End");
            isTesting = false;
            Save();
        }
    }


    public void startTesting()
    {
        startTest = true;
        testTime = Time.time + 5;
    }

    public void Save()
    {
        container2.Save(Path.Combine(Application.dataPath, "testing.xml"));
        Debug.Log("Saved");
    }

    public void Add()
    {
        Test test = new Test();
        test.time = Time.time;
        test.x = testingObject.transform.position.x;
        test.y = testingObject.transform.position.y;
        test.z = testingObject.transform.position.z;


        container2.Tests.Add(test);
        Debug.Log("Added");
    }
	
}
