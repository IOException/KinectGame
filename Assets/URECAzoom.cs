﻿using UnityEngine;
using System.Collections;


public class URECAzoom : MonoBehaviour
{ public float smooth;

    public bool legs;
        public bool arms;
        public bool armsprofile;
        public bool position;


    public GameObject BodyCam;
    
    private Vector3 player;
    private Vector3 Position;
    private Vector3 Legs;
    private Vector3 Arms;
    private Vector3 ArmsProfile;
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform.position;

     Vector3   View = BodyCam.transform.position;
      Position=  GameObject.FindGameObjectWithTag("PositionView").transform.position;
     Legs =  GameObject.FindGameObjectWithTag("LegsView").transform.position;
       Arms = GameObject.FindGameObjectWithTag("ArmsView").transform.position;
        ArmsProfile = GameObject.FindGameObjectWithTag("ArmsProfileView").transform.position;
     
    }
  void Update()
    {
        if (legs)
        {
            legsView();
        }


        else if (arms) {
            armsView();
        }
        else if (armsprofile){
            armsProfileView();
        }
        else if (position){
            positionView();
        }
        else { return; }

    }
      public void LookatLegs()
    { legs = true;
        armsprofile = false;
        arms = false;
        position = false;
    }
    public void LookatArms()
    {
        arms = true;
        armsprofile = false;
        position = false;
        legs = false;
    }
    public void LookatArmsProfile()
    {
        armsprofile = true;
        legs = false;
        arms = false;
        position = false;
    }
    public void LookatPosition()
    {
        position = true;
        armsprofile = false;
        arms = false;
       legs = false;
    }
    public void legsView()
    {
      Vector3  newPos = Legs;
        BodyCam.transform.position = Vector3.Lerp(BodyCam.transform.position, newPos, smooth*Time.deltaTime );
        SmoothLookAt();
        /* BodyCam.SetActive(false);
         LegCam.SetActive(true);
         ArmCam.SetActive(false);
         ArmProfileCam.SetActive(false);
     */
    }
      public void positionView() {

        Vector3 newPos = Position;
        BodyCam.transform.position = Vector3.Lerp(BodyCam.transform.position, newPos, smooth * Time.deltaTime);
        SmoothLookAt();

        /*BodyCam.SetActive(true);
            LegCam.SetActive(false);
            ArmCam.SetActive(false);
            ArmProfileCam.SetActive(false);
        */
    }
        public void armsView()
    {
        Vector3 newPos = Arms;
        BodyCam.transform.position = Vector3.Lerp(BodyCam.transform.position, newPos, smooth * Time.deltaTime);
        /*  BodyCam.SetActive(false);
          LegCam.SetActive(false);
          ArmCam.SetActive(true);
          ArmProfileCam.SetActive(false);
      */
        SmoothLookAt();
    }
    public void armsProfileView()
    {
        Vector3 newPos = ArmsProfile;
        BodyCam.transform.position = Vector3.Lerp(BodyCam.transform.position, newPos, smooth * Time.deltaTime);
      
   
        SmoothLookAt();
        /*BodyCam.SetActive(false);
        LegCam.SetActive(false);
        ArmCam.SetActive(false);
        ArmProfileCam.SetActive(true);
    */
    }
    void SmoothLookAt()
    {
        // Create a vector from the camera towards the player.
        Vector3 relPlayerPosition = player- BodyCam.transform.position;

        // Create a rotation based on the relative position of the player being the forward vector.
        Quaternion lookAtRotation = Quaternion.LookRotation(relPlayerPosition, Vector3.up);

        // Lerp the camera's rotation between it's current rotation and the rotation that looks at the player.
      transform.rotation = Quaternion.Lerp(transform.rotation, lookAtRotation, smooth * Time.deltaTime);
    }
}


