﻿using UnityEngine;
using System.Collections;
using System.IO;

public class UrecaArms : MonoBehaviour
{
    [Tooltip("The Kinect joint we want to track.")]
    public KinectInterop.JointType RHand = KinectInterop.JointType.HandRight;
    public KinectInterop.JointType RElbow = KinectInterop.JointType.ElbowRight;//set up script for these too
    public KinectInterop.JointType RShoulder = KinectInterop.JointType.ShoulderRight;//ditto

    public KinectInterop.JointType LHand = KinectInterop.JointType.HandLeft;
    public KinectInterop.JointType LElbow = KinectInterop.JointType.ElbowLeft;//set up script for these too
    public KinectInterop.JointType LShoulder = KinectInterop.JointType.ShoulderLeft;//yup
    bool RunUrecaArms;

    [Tooltip("Current joint position in Kinect coordinates (meters).")]
    public Vector3 RHandPosition;
    public Vector3 RElbowPosition;
    public Vector3 RShoulderPosition;

    public Vector3 LHandPosition;
    public Vector3 LElbowPosition;
    public Vector3 LShoulderPosition;

    public UnityEngine.UI.Text LtextUI;
    public UnityEngine.UI.Text RtextUI;


    float RPreviousAngle;
    float LPreviousAngle;

    float RHandSpeed;
    float LHandSpeed;

    [Tooltip("Whether we save the joint data to a CSV file or not.")]
    public bool isSaving = true;

    [Tooltip("Path to the CSV file, we want to save the joint data to.")]
    public string saveFilePath = "UrecaArms.csv";

    [Tooltip("How many seconds to save data to the CSV file, or 0 to save non-stop.")]
    public float secondsToSave = 0f;


    // start time of data saving to csv file
    private float saveStartTime = -1f;

  public  void RunUrecaArmsFunc(){ RunUrecaArms = true; }
    public void StopUrecaArmsFunc()
    { RunUrecaArms = false; }


    void Start()
    {
        RHandSpeed = 0;
        LHandSpeed = 0;
        RPreviousAngle = 0;
        LPreviousAngle = 0;
        if (isSaving && File.Exists(saveFilePath))
        {
            File.Delete(saveFilePath);

        }
    }


    void Update()
    {
        if (isSaving)
        {
            // create the file, if needed
            if (!File.Exists(saveFilePath))
            {
                using (StreamWriter writer = File.CreateText(saveFilePath))
                {
                    // csv file header
                    string sLine = "time, RHand,pos_x,pos_y,poz_z, RElbow,pos_x,pos_y,poz_z, RShoulder,pos_x,pos_y,poz_z, LHand,pos_x,pos_y,poz_z, LElbow,pos_x,pos_y,poz_z, LShoulder,pos_x,pos_y,poz_z,RightArmAngles,RightArmChangeInAngles,LeftArmAngles,LeftArmChangeInAngles,RHandSpeed,LHandSpeed";
                    writer.WriteLine(sLine);
                }
            }

            // check the start time
            if (saveStartTime < 0f)
            {
                saveStartTime = Time.time;
            }
        }

        // get the joint position
        KinectManager manager = KinectManager.Instance;

        if (manager && manager.IsInitialized())
        {
            if (manager.IsUserDetected() && RunUrecaArms)
            {
                long userId = manager.GetPrimaryUserID();
                //Debug.Log("Here");
                if (manager.IsJointTracked(userId, (int)RHand)&&
                    manager.IsJointTracked(userId, (int)RElbow) &&
                    manager.IsJointTracked(userId, (int)RShoulder) &&
                    manager.IsJointTracked(userId, (int)LHand) &&
                    manager.IsJointTracked(userId, (int)LElbow) &&
                    manager.IsJointTracked(userId, (int)LShoulder))
                {
                    // output the joint position for easy tracking
                    Vector3 RHandPos = manager.GetJointPosition(userId, (int)RHand);
                     RHandSpeed = (RHandPos.magnitude - RHandPosition.magnitude) / Time.deltaTime;
                    RHandPosition = RHandPos;
                //    Debug.Log("R:"+RHandSpeed);
                    Vector3 RElbowPos = manager.GetJointPosition(userId, (int)RElbow);
                    RElbowPosition = RElbowPos;

                    Vector3 RShoulderPos = manager.GetJointPosition(userId, (int)RShoulder);
                    RShoulderPosition = RShoulderPos;

                    Vector3 LHandPos = manager.GetJointPosition(userId, (int)LHand);
                     LHandSpeed = (LHandPos.magnitude - LHandPosition.magnitude) / Time.deltaTime;
                    LHandPosition = LHandPos;

                    Vector3 LElbowPos = manager.GetJointPosition(userId, (int)LElbow);
                    LElbowPosition = LElbowPos;

                    Vector3 LShoulderPos = manager.GetJointPosition(userId, (int)LShoulder);
                    LShoulderPosition = LShoulderPos;


                   //Angle Calculation Components
                    Vector3 LeftUpperArm= -LElbowPos+LShoulderPos;

                    Vector3 LeftForeArm = LHandPos-LElbowPos;

                    Vector3 RightUpperArm= -RElbowPos+RShoulderPos;
                    
                    Vector3 RightForeArm=RHandPos-RElbowPos;
                    //Calculating Angles
                    float LeftArmAngle=Vector3.Angle(LeftForeArm,LeftUpperArm);
                    float RightArmAngle=Vector3.Angle(RightForeArm, RightUpperArm);



                    //Calculating Change in Angles
                    float RightArmChangeInAngle = (RightArmAngle - RPreviousAngle) / Time.deltaTime;
                    float LeftArmChangeInAngle = (LeftArmAngle - LPreviousAngle) / Time.deltaTime;

                    RPreviousAngle = RightArmAngle;
                    LPreviousAngle = LeftArmAngle;
                    LtextUI.text = LeftArmAngle.ToString();
                    RtextUI.text = RightArmAngle.ToString();
                   // Debug.Log(LeftArmAngle);
                    // Debug.Log(RightArmAngle);
                    if (isSaving)
                    {
                        if ((secondsToSave == 0f) || ((Time.time - saveStartTime) <= secondsToSave))
                        {
                            using (StreamWriter writer = File.AppendText(saveFilePath))
                            {
                                string sLine = string.Format("{0:F3},{1},{2:F3},{3:F3},{4:F3}, {5},{6:F3},{7:F3},{8:F3},     {9},{10:F3},{11:F3},{12:F3},  {13},{14:F3},{15:F3},{16:F3}, {17},{18:F3},{19:F3},{20:F3} ,{21},{22:F3},{23:F3},{24:F3},{25:F3},{26:F3},{27:F3},{28:F3},{29:F3},{30:F3}", 
                               Time.time, ((KinectInterop.JointType)RHand).ToString(), RHandPos.x, RHandPos.y, RHandPos.z, ((KinectInterop.JointType) RElbow).ToString(), RElbowPos.x, RElbowPos.y, RElbowPos.z,((KinectInterop.JointType)RShoulder).ToString(), RShoulderPos.x, RShoulderPos.y, RShoulderPos.z,((KinectInterop.JointType)LHand).ToString(), LHandPos.x, LHandPos.y, LHandPos.z, ((KinectInterop.JointType)LElbow).ToString(), LElbowPos.x, LElbowPos.y, LElbowPos.z, ((KinectInterop.JointType)LShoulder).ToString(), LShoulderPos.x, LShoulderPos.y, LShoulderPos.z,RightArmAngle,RightArmChangeInAngle,LeftArmAngle,LeftArmChangeInAngle,RHandSpeed,LHandSpeed);
                                writer.WriteLine(sLine);
                            }
                        }
                    }
                }
            }
        }

    }

}
