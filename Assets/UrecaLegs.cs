﻿using UnityEngine;
using System.Collections;
using System.IO;

public class UrecaLegs : MonoBehaviour
{
    [Tooltip("The Kinect joint we want to track.")]
    public KinectInterop.JointType RFoot = KinectInterop.JointType.FootRight;
    public KinectInterop.JointType RKnee = KinectInterop.JointType.KneeRight;//set up script for these too
    public KinectInterop.JointType RHip = KinectInterop.JointType.HipRight;//ditto

    public KinectInterop.JointType LFoot = KinectInterop.JointType.FootLeft;
    public KinectInterop.JointType LKnee = KinectInterop.JointType.KneeLeft;//set up script for these too
    public KinectInterop.JointType LHip = KinectInterop.JointType.HipLeft;//yup
    bool RunUrecaLegs;

    [Tooltip("Current joint position in Kinect coordinates (meters).")]
    public Vector3 RFootPosition;
    public Vector3 RKneePosition;
    public Vector3 RHipPosition;

    public Vector3 LFootPosition;
    public Vector3 LKneePosition;
    public Vector3 LHipPosition;

    public UnityEngine.UI.Text LtextUI;
    public UnityEngine.UI.Text RtextUI;


    float RPreviousAngle;
    float LPreviousAngle;
    float RFootSpeed;
    float LFootSpeed;

   
    [Tooltip("Whether we save the joint data to a CSV file or not.")]
    public bool isSaving = true;

    [Tooltip("Path to the CSV file, we want to save the joint data to.")]
    public string saveFilePath = "UrecaLegs.csv";

    [Tooltip("How many seconds to save data to the CSV file, or 0 to save non-stop.")]
    public float secondsToSave = 0f;


    // start time of data saving to csv file
    private float saveStartTime = -1f;

    public void RunUrecaLegsFunc()
    { RunUrecaLegs = true; }
    public void StopUrecaLegsFunc()
    { RunUrecaLegs = false; }
    
    void Start()
    {
        RFootSpeed=0;
        LFootSpeed = 0;
        RPreviousAngle = 0;
        LPreviousAngle = 0;
        if (isSaving && File.Exists(saveFilePath))
        {
            File.Delete(saveFilePath);

        }
    }


    void Update()
    {
        if (isSaving)
        {
            // create the file, if needed
            if (!File.Exists(saveFilePath))
            {
                using (StreamWriter writer = File.CreateText(saveFilePath))
                {
                    // csv file header
                    string sLine = "time, RFoot,pos_x,pos_y,poz_z, RKnee,pos_x,pos_y,poz_z, RHip,pos_x,pos_y,poz_z, LFoot,pos_x,pos_y,poz_z, LKnee,pos_x,pos_y,poz_z, LHip,pos_x,pos_y,poz_z,RightLegAngles,RightLegChangeInAngles,LeftLegAngles,LeftLegChangeInAngles,RFootSpeed,LFootSpeed";
                    writer.WriteLine(sLine);
                }
            }

            // check the start time
            if (saveStartTime < 0f)
            {
                saveStartTime = Time.time;
            }
        }

        // get the joint position
        KinectManager manager = KinectManager.Instance;

        if (manager && manager.IsInitialized())
        {
            if (manager.IsUserDetected() && RunUrecaLegs)
            {
                long userId = manager.GetPrimaryUserID();
                Debug.Log("Here");
                if (manager.IsJointTracked(userId, (int)RFoot) &&
                    manager.IsJointTracked(userId, (int)RKnee) &&
                    manager.IsJointTracked(userId, (int)RHip) &&
                    manager.IsJointTracked(userId, (int)LFoot) &&
                    manager.IsJointTracked(userId, (int)LKnee) &&
                    manager.IsJointTracked(userId, (int)LHip))
                {
                    // output the joint position for easy tracking
                    Vector3 RFootPos = manager.GetJointPosition(userId, (int)RFoot);
                    RFootSpeed = (RFootPos.magnitude - RFootPosition.magnitude) / Time.deltaTime;// this is wrong. break it up into components
                    RFootPosition = RFootPos;

                    Vector3 RKneePos = manager.GetJointPosition(userId, (int)RKnee);
                    RKneePosition = RKneePos;

                    Vector3 RHipPos = manager.GetJointPosition(userId, (int)RHip);
                    RHipPosition = RHipPos;

                    Vector3 LFootPos = manager.GetJointPosition(userId, (int)LFoot);
                    LFootSpeed =( LFootPos.magnitude - LFootPosition.magnitude) / Time.deltaTime;
                    LFootPosition = LFootPos;

                    Vector3 LKneePos = manager.GetJointPosition(userId, (int)LKnee);
                    LKneePosition = LKneePos;

                    Vector3 LHipPos = manager.GetJointPosition(userId, (int)LHip);
                    LHipPosition = LHipPos;




                    //Angle Calculation Components
                    Vector3 LeftUpperLeg =  LHipPos- LKneePos;

                    Vector3 LeftForeLeg = LFootPos - LKneePos;

                    Vector3 RightUpperLeg =  RHipPos- RKneePos;

                    Vector3 RightForeLeg = RFootPos - RKneePos;
                    //Calculating Angles
              float       LeftLegAngle = Vector3.Angle(LeftForeLeg, LeftUpperLeg);
             float        RightLegAngle = Vector3.Angle(RightForeLeg, RightUpperLeg);



                    //Calculating Change in Angles
                    float RightLegChangeInAngle = RightLegAngle - RPreviousAngle / Time.deltaTime;
                    float LeftLegChangeInAngle = LeftLegAngle - LPreviousAngle / Time.deltaTime;

                    RPreviousAngle = RightLegAngle;
                    LPreviousAngle = LeftLegAngle;
                    LtextUI.text = LeftLegAngle.ToString();
                    RtextUI.text = RightLegAngle.ToString();
                    Debug.Log(LeftLegChangeInAngle);
                    Debug.Log(RightLegChangeInAngle);
                    if (isSaving)
                    {
                        if ((secondsToSave == 0f) || ((Time.time - saveStartTime) <= secondsToSave))
                        {
                            using (StreamWriter writer = File.AppendText(saveFilePath))
                            {
                                string sLine = string.Format("{0:F3},{1},{2:F3},{3:F3},{4:F3}, {5},{6:F3},{7:F3},{8:F3},     {9},{10:F3},{11:F3},{12:F3},  {13},{14:F3},{15:F3},{16:F3}, {17},{18:F3},{19:F3},{20:F3} ,{21},{22:F3},{23:F3},{24:F3},{25:F3},{26:F3},{27:F3},{28:F3},{29:F3},{30:F3}",
                                Time.time, ((KinectInterop.JointType)RFoot).ToString(), RFootPos.x, RFootPos.y, RFootPos.z, ((KinectInterop.JointType)RKnee).ToString(), RKneePos.x, RKneePos.y, RKneePos.z, ((KinectInterop.JointType)RHip).ToString(), RHipPos.x, RHipPos.y, RHipPos.z, ((KinectInterop.JointType)LFoot).ToString(), LFootPos.x, LFootPos.y, LFootPos.z, ((KinectInterop.JointType)LKnee).ToString(), LKneePos.x, LKneePos.y, LKneePos.z, ((KinectInterop.JointType)LHip).ToString(), LHipPos.x, LHipPos.y, LHipPos.z, RightLegAngle, RightLegChangeInAngle, LeftLegAngle, LeftLegChangeInAngle, RFootSpeed, LFootSpeed);
                                writer.WriteLine(sLine);
                            }
                        }
                    }
                }
            }
        }

    }

}
