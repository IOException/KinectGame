﻿using UnityEngine;
using System.Collections;

public class CombatEnemyController : MonoBehaviour
{
    public Animator animator;
    GameObject player;
    public float speed;
    public int health;
    public float attackTimer;
    public float distance;
    public bool die;
    bool attackting = false;
    float attackTime;

    public GameObject gameController;
    // Use this for initialization
    void Start()
    {
        die = false;
        player = GameObject.FindGameObjectWithTag("Player");
        gameController = GameObject.FindGameObjectWithTag("GameController");
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (die == false)
        {
            transform.LookAt(player.transform);
            if (health <= 0)
            {
                animator.SetBool("Die", true);
                Destroy(gameObject, 2);
                die = true;
            }
            if (Vector3.Distance(player.transform.position, gameObject.transform.position) > distance)
            {
                gameObject.GetComponent<Rigidbody>().velocity = (player.transform.position - gameObject.transform.position).normalized * speed;
                if (attackTime >= Time.time)
                {
                    attackting = false;
                }
            }
            else
            {
                gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                if (attackting == false)
                {
                    Attack();
                }
                else
                {
                    if (attackTime <= Time.time)
                    {
                        if (attackting == true)
                        {

                            gameController.GetComponent<CombatGameController>().hp--;
                        }
                        attackting = false;
                    }
                }
                
            }

        }
        else
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }


    }


    public void takeDamage(int damage)
    {
        health -= damage;
    }


    public bool getDie()
    {
        return die;
    }

    void Attack()
    {
        animator.SetBool("Idle",true);
        animator.SetTrigger("Attack");
        attackting = true;
        attackTime = Time.time+ attackTimer;
    }
}
