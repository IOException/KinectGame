﻿using UnityEngine;
using System.Collections;

public class CombatGameController : MonoBehaviour {
    public GameObject spawnPosition;
    public GameObject spawnPosition2;

    public GameObject enemy;
    public GameObject enemy2;
    bool start;
    float time;
    public float waitTime;
    public GameObject HealthUI;
    public int hp;
    // Use this for initialization
    void Start () {
        start = true;
        time = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if (hp <= 0)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
        HealthUI.GetComponent<HealthUIController>().hp = hp;
        if (Time.time > time && start == true)
        {
            if (Random.Range(0, 4f) < 1)
            {
                if (Random.Range(0, 1.99f) < 1)
                {
                    Instantiate(enemy, spawnPosition.transform.position, Quaternion.identity);

                }
                else
                {
                    Instantiate(enemy, spawnPosition2.transform.position, Quaternion.identity);

                }
            }
            else
            {

                if (Random.Range(0, 1.99f) < 1)
                {
                    Instantiate(enemy2, spawnPosition.transform.position, Quaternion.identity);

                }
                else
                {
                    Instantiate(enemy2, spawnPosition2.transform.position, Quaternion.identity);

                }
            }
            time = Time.time + waitTime;
        }

    }
}
