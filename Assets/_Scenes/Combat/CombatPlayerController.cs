﻿using UnityEngine;
using System.Collections;

public class CombatPlayerController : MonoBehaviour
{
    public GameObject LeftHand;
    public GameObject LeftElbow;
    public GameObject LeftShoulder;
    public GameObject RightHand;
    public GameObject RightElbow;
    public GameObject RightShoulder;

    public int attackRange;

    public Animator animator;
    GameObject Player;
    GameObject Camera;
    Vector3 CameraPosition;

    public bool LeftArmReady;
    public   bool RightArmReady;

    float LeftArmTimer;
    float RightArmTimer;


    Vector3 CameraVecocity = Vector3.zero;
    // Use this for initialization
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        Camera = GameObject.FindGameObjectWithTag("MainCamera");
        CameraPosition = Camera.transform.position;
        animator = Player.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Camera.transform.position = Vector3.SmoothDamp(Camera.transform.position, new Vector3(CameraPosition.x + Player.transform.position.x, CameraPosition.y, CameraPosition.z), ref CameraVecocity, 0.4f);
        Vector3 LeftForeArm = LeftHand.transform.position - LeftElbow.transform.position;
        Vector3 LeftUpperArm = LeftShoulder.transform.position - LeftElbow.transform.position;

        Vector3 RightForeArm = RightHand.transform.position - RightElbow.transform.position;
        Vector3 RightUpperArm = RightShoulder.transform.position - RightElbow.transform.position;

        float LeftArmAngle = Vector3.Angle(LeftForeArm, LeftUpperArm);
        float RightArmAngle = Vector3.Angle(RightForeArm, RightUpperArm);

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Debug.Log("left");
            AttackLeft();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Debug.Log("right");
            AttackRight();
        }


        if (LeftArmAngle < 90.0F)
        {
            LeftArmReady = true;
            LeftArmTimer = Time.time;
        }
        if (RightArmAngle < 90.0F)
        {
            RightArmReady = true;
            RightArmTimer = Time.time;
        }
        if (Time.time - LeftArmTimer > 2)
        {
            LeftArmReady = false;
        }
        if (Time.time - RightArmTimer > 2)
        {
            RightArmReady = false;
        }
        if (LeftArmReady == true && LeftArmAngle > 130)
        {
            if (Mathf.Abs(LeftHand.transform.position.y - LeftShoulder.transform.position.y) < 0.4)
            {
                if (LeftShoulder.transform.position.x - LeftHand.transform.position.x < 0)
                {
                    //Attack Left
                    AttackLeft();
                    LeftArmReady = false;
                }
                else
                {
                    //Attack Right
                    AttackRight();
                    LeftArmReady = false;
                }
            }
        }

        if (RightArmReady == true && RightArmAngle > 130)
        {
            if (Mathf.Abs(RightHand.transform.position.y - RightShoulder.transform.position.y) < 0.4)
            {
                if (RightShoulder.transform.position.x - RightHand.transform.position.x > 0)
                {
                    //Attack Right
                    AttackRight();
                    RightArmReady = false;
                }
                else
                {
                    //Attack Left
                    AttackLeft();
                    RightArmReady = false;
                }
            }
        }
    }

    void AttackLeft()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        Debug.Log(enemies.Length);
        if (enemies.Length == 0)
        {
            return;
        }
        for (int i = 0; i < enemies.Length; i++)
        {
            if ((Vector3.Distance(Player.transform.position, enemies[i].transform.position) < attackRange && (Player.transform.position.x - enemies[i].transform.position.x) < 0) && enemies[i].GetComponent<CombatEnemyController>().getDie()==false)
            {
                if (enemies[i].GetComponent<CombatEnemyController>().getDie() == false)
                {
                    Player.transform.position = new Vector3(enemies[i].transform.position.x - 1.5f, Player.transform.position.y, Player.transform.position.z);
                    Player.transform.rotation = Quaternion.Euler(0, 90, 0);
                    enemies[i].GetComponent<CombatEnemyController>().takeDamage(1);
                    animator.SetTrigger("PunchTrigger");
                    break;
                }

            }
        }
    }

    void AttackRight()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        Debug.Log(enemies.Length);
        if (enemies.Length==0)
        {
            return;
        }
        for (int i = 0; i < enemies.Length; i++)
        {
            if (Vector3.Distance(Player.transform.position, enemies[i].transform.position) < attackRange && (Player.transform.position.x - enemies[i].transform.position.x) > 0 )
            {
                if(enemies[i].GetComponent<CombatEnemyController>().die == false)
                {
                    Player.transform.position = new Vector3(enemies[i].transform.position.x + 1.5f, Player.transform.position.y, Player.transform.position.z);
                    Player.transform.rotation = Quaternion.Euler(0, 270, 0);
                    enemies[i].GetComponent<CombatEnemyController>().takeDamage(1);
                    animator.SetTrigger("PunchTrigger");
                    break;
                }

            }
        }
    }


}
