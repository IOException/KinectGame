﻿using UnityEngine;
using System.Collections;

public class CombatSpawnPosition : MonoBehaviour {
    public GameObject player;
    Vector3 position;
	// Use this for initialization
	void Start () {
        position = gameObject.transform.position;

    }
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.position = player.transform.position+ position;
    }
}
