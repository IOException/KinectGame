﻿using UnityEngine;
using System.Collections;

public class GripController : MonoBehaviour {

    public GameObject LeftHand;
    public GameObject RightHand;
    public GameObject Cube;

    Renderer CubeRenderer;

    bool LeftHandGrip;
    bool RightHandGrip;


    private InteractionManager manager;

	// Use this for initialization
	void Start () {
        CubeRenderer = Cube.GetComponent<Renderer>();
	
	}
	
	// Update is called once per frame
	void Update () {
        if (manager == null)
        {
            manager = InteractionManager.Instance;
        }


        if (manager != null && manager.IsInteractionInited())
        {
            if (manager.GetLastLeftHandEvent() == InteractionManager.HandEventType.Grip)
            {
                LeftHandGrip = true;
            }
            else
            {
                LeftHandGrip = false;
            }

            if (manager.GetLastRightHandEvent() == InteractionManager.HandEventType.Grip)
            {
                RightHandGrip = true;
            }
            else
            {
                RightHandGrip = false;
            }
        }

        if(RightHandGrip==true&&LeftHandGrip==true)
        {
            if(LeftHand.GetComponent<LeftHand>().Collide==true&& RightHand.GetComponent<RightHand>().Collide == true)
            CubeRenderer.material.color = Color.green;
            Cube.transform.position = new Vector3((LeftHand.transform.position.x + RightHand.transform.position.x) / 2, (LeftHand.transform.position.y + RightHand.transform.position.y) / 2, (LeftHand.transform.position.z + RightHand.transform.position.z) / 2);
        }
        else
        {
            CubeRenderer.material.color = Color.red;
        }
    }
}
