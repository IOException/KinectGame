using UnityEngine;
using System.Collections;

public class GrabDropScript2 : MonoBehaviour
{

	[Tooltip("List of the objects that may be dragged and dropped.")]
	public GameObject[]
		draggableObjects;

	[Tooltip("Material used to outline the currently selected object.")]
	public Material selectedObjectMaterial;
    public GameObject Cube;

	// interaction manager reference
	private InteractionManager manager;
	private bool isLeftHandDrag;

	// currently dragged object and its parameters
	private GameObject draggedObject;
	private Material draggedObjectMaterial;
    
	void Start ()
	{
	}

	void Update ()
	{

		// get the interaction manager instance
		if (manager == null) {
			manager = InteractionManager.Instance;
		}

        Debug.Log(manager.IsInteractionInited());

		if (manager != null && manager.IsInteractionInited ()) {
			
			if (draggedObject == null) {
				// if there is a hand grip, select the underlying object and start dragging it.
					// if the left hand is primary, check for left hand grip
					if (manager.GetLastLeftHandEvent () == InteractionManager.HandEventType.Grip) {
						isLeftHandDrag = true;
					}
				} else
					// if the right hand is primary, check for right hand grip
					if (manager.GetLastRightHandEvent () == InteractionManager.HandEventType.Grip) {
						isLeftHandDrag = false;
					}
				}
			
			}
		
	

	
	
}
