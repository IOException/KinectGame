﻿using UnityEngine;
using System.Collections;

public class OldMenuController : MonoBehaviour {

    public GameObject Camera;
    public GameObject StartScreen;
    public GameObject PlayerMenu;
    public GameObject GameMenu;
    public GameObject NewPlayerMenu;

    public GameObject LeftHand;
    public GameObject RightHand;
    public GameObject Head;
    bool startScreen;
	// Use this for initialization
	void Start () {
        /**
        StartScreen.gameObject.SetActive(true);
        startScreen = true;
        Camera.transform.position = new Vector3(0, 0, -10);
        **/

        PlayerMenu.gameObject.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
        /**
        if (startScreen==true&&LeftHand.transform.position.y > Head.transform.position.y && RightHand.transform.position.y > Head.transform.position.y)
        {
            StartScreen.gameObject.SetActive(false);
            startScreen = false;
        }
    **/
	
	}

    public void SelectPlayerContinue()
    {       
            PlayerMenu.gameObject.SetActive(false);
            GameMenu.gameObject.SetActive(true);
    }

    public void SelectGameMap()
    {
        GameMenu.gameObject.SetActive(false);
        Camera.transform.position = new Vector3(30.44f, 0, -10);
    }

    public void SelectNewPlayer()
    {
        PlayerMenu.gameObject.SetActive(false);
        NewPlayerMenu.gameObject.SetActive(true);
    }

    public void NewPlayerConfirm()
    {
        NewPlayerMenu.gameObject.SetActive(false);
        PlayerMenu.gameObject.SetActive(true);
    }
}
