﻿using System.Xml;
using System.Xml.Serialization;

public class Player{

    [XmlAttribute("Name")]
    public string name;
    public string age;
}
