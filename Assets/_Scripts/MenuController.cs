﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

    public GameObject MainScreen;
    public GameObject CalibrationScreen;
    public GameObject NewPlayerScreen;
    public GameObject PlayerList;
    public GameObject camera;


   
    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	    
	}
    public void StartLevel1()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }
    public void StartLevel2()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);
    }
    public void StartLevel3()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(3);
    }
    public void StartLevel4()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(4);
    }
    public void StartLevel5()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(5);
    }

    public void CreatPlayerToMainScreen()
    {
        NewPlayerScreen.SetActive(false);
        MainScreen.SetActive(true);
        camera.transform.position = new Vector3(0, 1, -10);
        camera.transform.rotation = Quaternion.Euler(0, -360, 0);
    }

    public void MainScreenToNewPlayerScreen()
    {
        camera.transform.position = new Vector3(-2f, 0.3f, -4f);
        camera.transform.rotation = Quaternion.Euler(0, 180, 0);
        NewPlayerScreen.SetActive(true);
        MainScreen.SetActive(false);
        
    }

    public void MainScreenToCalibrationScreen()
    {
        camera.transform.position = new Vector3(0, 1, -10);
        MainScreen.SetActive(false);
        CalibrationScreen.SetActive(true);
    }

    public void PlayerListToMainScreen()
    {
        PlayerList.SetActive(false);
        MainScreen.SetActive(true);
    }

    public void MainScreenToPlayerList()
    {
        PlayerList.SetActive(true);

        gameObject.GetComponent<PlayerList>().OpenList();
        MainScreen.SetActive(false);
    }
}
