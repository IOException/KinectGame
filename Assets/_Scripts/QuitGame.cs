﻿using UnityEngine;
using System.Collections;

/**

	This script allows the user to exit the game by pressing the ESC key

	This script is intended for DEBUGGING purposes

*/
public class QuitGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}
	
	}
}
