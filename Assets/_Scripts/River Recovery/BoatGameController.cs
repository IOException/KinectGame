﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.SceneManagement;

public class BoatGameController : MonoBehaviour
{
    public enum GameState
    {
        Pregame, Paused, Running, EndLose, EndWin
    }

    [SerializeField]
    private GameState _state = GameState.Pregame;

    public GameState State
    {
        get { return _state; }
        private set { _state = value; }
    }

    private static BoatGameController _instance;
    public static BoatGameController Instance
    {
        get { return _instance; }
    }

    public int Score { get; set; }

    private BoatInputController _input;
    public BoatPlayerController Player;

    /// <summary>
    /// 
    /// </summary>
    void Awake()
    {
        _instance = this;
    }

    /// <summary>
    /// 
    /// </summary>
    void Start()
    {
        Reset();
    }

    /// <summary>
    /// 
    /// </summary>
    void Reset()
    {
        State = GameState.Pregame;
        Score = 0;
        _input = BoatInputController.Instance;
        Player = FindObjectOfType<BoatPlayerController>();
    }

    /// <summary>
    /// 
    /// </summary>
    private void Update()
    {
        // dead?
        if (State != GameState.EndLose && Player.State == PlayerState.Dead)
        {
            EndGame(false);
            return;
        }

        _input.HandleInput();
    }

    /// <summary>
    /// 
    /// </summary>
    private void EndGame(bool success)
    {
        State = success ? GameState.EndWin : GameState.EndLose;
        if (success)
        {
            UiController.Instance.ShowVictoryPanel();
        }
        else
        {
            UiController.Instance.ShowGameOverPanel();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void StartGame()
    {
        Reset();
        // tell objects to start moving
        State = GameState.Running;
        UiController.Instance.HidePregamePanel();
    }

    /// <summary>
    /// 
    /// </summary>
    public void HandleLeft()
    {
        Player.GoLeft();
    }

    /// <summary>
    /// 
    /// </summary>
    public void HandleRight()
    {
        Player.GoRight();
    }

    /// <summary>
    /// 
    /// </summary>
    public void HandleForward()
    {
        Player.GoForward();
    }

    /// <summary>
    /// 
    /// </summary>
    public void Pause()
    {
        // TODO; bring up pausemenu, stop clock
        State = GameState.Paused;
    }

    /// <summary>
    /// 
    /// </summary>
    public void Unpause()
    {
        // TODO hide pause menu
        State = GameState.Running;
    }

    /// <summary>
    /// Reloads current cene
    /// </summary>
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// when the player reaches the end of the level, the "goal"
    /// </summary>
    public void GoalReached()
    {
        EndGame(true);
    }

    /// <summary>
    /// We're done here. @David, let's go to the next level~
    /// 
    /// David: going back to map for now
    /// </summary>
    public void GoToNextLevel()
    {
        SceneManager.LoadScene(0);
        //SceneManager.LoadScene(Scenes.Sharpshooter);
    }
}
