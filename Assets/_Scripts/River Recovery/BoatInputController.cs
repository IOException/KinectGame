﻿using System;
using UnityEngine;

/// <summary>
/// Just using keyboard input for now -- implement kinect input later
/// </summary>
public class BoatInputController : MonoBehaviour
{
    private BoatGameController _gc;
    private static BoatInputController _instance;
    public static BoatInputController Instance { get { return _instance; } }

    void Awake()
    {
        _instance = this;
    }

    // Use this for initialization
    void Start()
    {
        _gc = BoatGameController.Instance;
    }

    public void HandleInput()
    {
        // TODO: better key setup; config?
        switch (_gc.State)
        {
            case BoatGameController.GameState.Pregame:
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    _gc.StartGame();
                }
                break;
            case BoatGameController.GameState.Running:
                // Left
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    _gc.HandleLeft();
                }
                // Right
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    _gc.HandleRight();
                }
                // Up (forward)
                else if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    _gc.HandleForward();
                }
                // Escape (menu)
                else if (Input.GetKeyDown(KeyCode.Escape))
                {
                    _gc.Pause();
                }
                else if (Input.GetKeyDown(KeyCode.X))
                {
                    _gc.Player.TakeDamage(1);
                }
                break;
            case BoatGameController.GameState.Paused:
                // Press Escape to resume
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    _gc.Unpause();
                }
                // TODO menu? should I be able to quit from here?
                break;
            case BoatGameController.GameState.EndLose:
                // press R to restart
                if (Input.GetKeyDown(KeyCode.R))
                {
                    _gc.RestartGame();
                }
                break;
            case BoatGameController.GameState.EndWin:
                // press Enter to go to next level
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    _gc.GoToNextLevel();
                }
                break;
            default:
                throw new Exception("Unknown Gamestate");
        }
    }
}
