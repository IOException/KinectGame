﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Pretty much just handles terrain collision
/// </summary>
public class BounceOffTerrain : MonoBehaviour
{
    public float kbForce = 1.0f;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(Tags.Terrain))
        {
            var rb = GetComponent<Rigidbody>();

            var norm = collision.contacts[0].normal;
            // take the vertical component out 
            norm.y = 0;
            norm.Normalize();

            var nerfedSpeed = rb.velocity.magnitude * .9f;
            var newVel = rb.velocity.normalized * nerfedSpeed;

            var bounce = Vector3.Reflect(newVel, norm);

            var force = bounce * kbForce;
            GetComponent<Rigidbody>().AddForce(force);
        }
    }
}
