﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class ClampMaxSpeed : MonoBehaviour
{
    public float maxSpeed = 200f;

    void FixedUpdate()
    {
        var _rb = GetComponent<Rigidbody>();
        if (_rb.velocity.magnitude > maxSpeed)
        {
            _rb.velocity = _rb.velocity.normalized * maxSpeed;
        }
    }
}
