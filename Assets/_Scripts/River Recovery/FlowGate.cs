﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A flow gate has child objects From and To that help determine the change in flow direction
/// </summary>
[RequireComponent(typeof(BoxCollider))]
public class FlowGate : MonoBehaviour
{
    public float speed;
    private bool canPush = true;
    private static float pushCd = .0167f;

    Vector3 direction
    {
        get
        {
            var f = transform.forward;
            f.y = 0;
            return f.normalized * speed;
        }
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay(Collider other)
    {
        if (BoatGameController.Instance.State != BoatGameController.GameState.Running)
            return;

        if (canPush)
        {
            var rb = other.GetComponent<Rigidbody>();

            // only let the zone push an object up to 60% of its max clamped speed
            if (other.GetComponent<ClampMaxSpeed>() != null)
            {
                var clamp = other.GetComponent<ClampMaxSpeed>();
                if ((rb.velocity.magnitude * .6) >= clamp.maxSpeed)
                {
                    // only turn to face new dir
                    /*rb.transform.rotation = Quaternion.Slerp(
                        transform.rotation,
                        Quaternion.LookRotation(direction),
                        Time.deltaTime * 1
                        );*/
                    return;
                }
            }

            rb.AddForce(direction);
            StartCoroutine(PushCoolDown());
        }
    }

    IEnumerator PushCoolDown()
    {
        canPush = false;

        yield return new WaitForSeconds(pushCd);

        canPush = true;
    }

    void OnDrawGizmos()
    {
        var _col = GetComponent<BoxCollider>();
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(_col.center, _col.size);
    }
}