﻿using UnityEngine;
using System.Collections;

public class LookAtVelocity : MonoBehaviour
{
    private float rotationSpeed = 1.0f;

    void FixedUpdate()
    {
        var dir = GetComponent<Rigidbody>().velocity;
        if (dir != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(
                transform.rotation,
                Quaternion.LookRotation(dir),
                Time.deltaTime * rotationSpeed
            );
        }
    }
}
