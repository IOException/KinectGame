﻿using UnityEngine;
using System.Collections;

public class SpawnPrefab : MonoBehaviour
{
    public static SpawnPrefab Instance;

    void Awake()
    {
        Instance = this;
    }

    // Externally handles spawning of prefabs
    public void Spawn(GameObject obj, Vector3 pos, Quaternion quat)
    {
        Instantiate(obj, pos, quat);
    }
}
