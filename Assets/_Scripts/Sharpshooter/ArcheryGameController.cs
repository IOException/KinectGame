﻿using UnityEngine;
using System.Collections;

/**
 * <summary>
 * ArcheryGameController manages the game state for the level "Sharpshooter," including position of the player and
 * difficulty of the various waves.
 * </summary>
 */
public class ArcheryGameController : MonoBehaviour {

	[Tooltip("The Player object within the level")]
	public GameObject player;

	//ACESSING OTHER SCRIPTS
	[Tooltip("The GameObject that contains the scripts that control the game")]
	public GameObject CONTROLLERS;
	private ArcheryEnemyController _ec;

	//CONSTANTS
	private const int TELEPORT = 100000;
	private const int NUM_LOCATIONS = 6;

	private const int FIRST_POSITION = 1;
	private const int SECOND_POSITION = 2;
	private const int THIRD_POSITION = 3;
	private const int FOURTH_POSITION = 4;
	private const int FIFTH_POSITION = 5;
	private const int SIXTH_POSITION = 6;


	//---Gameplay constants
	private const int MAX_WAVES = 5;
	private int numEnemies_default = 42;

	[Tooltip("The number of enemies in the 1st wave")]
	public int numEnemies_wave1 = 25;

	[Tooltip("The number of enemies in the 2nd wave")]
	public int numEnemies_wave2 = 50;

	[Tooltip("The number of enemies in the 3rd wave")]
	public int numEnemies_wave3 = 75;

	[Tooltip("The number of enemies in the 4th wave")]
	public int numEnemies_wave4 = 100;

	[Tooltip("The number of enemies in the 5th wave")]
	public int numEnemies_wave5 = 125;

	//PUBLIC VARIABLES

	//---PLAYER LOCATION VECTORS
	[Tooltip("The 1st position from which the player can shoot")]
	public Transform location_1;

	[Tooltip("The 2nd position from which the player can shoot")]
	public Transform location_2;

	[Tooltip("The 3rd position from which the player can shoot")]
	public Transform location_3;

	[Tooltip("The 4th position from which the player can shoot")]
	public Transform location_4;

	[Tooltip("The 5th position from which the player can shoot")]
	public Transform location_5;

	[Tooltip("The 6th position from which the player can shoot")]
	public Transform location_6;

	//PRIVATE VARIABLES
	private int playerLocation;
	private int wave_counter;

	//---Timer variables
	private float game_clock;
	private float wave_break_timer;

	[Tooltip("The amount of time (in seconds) between each wave of enemies")]
	public float wave_break_length;

	//CONTROL VARIABLES
	private bool isBetweenWaves;
	private bool isPaused;

	//VIEWING VARIABLES
	[Tooltip("(DEBUG) VIEW VARIABLE\nAllows the variable's value to be viewed during the script's execution.")]
	public int view_player_location;

	void Start () {
		if (player == null) {
			player = GameObject.FindWithTag ("Player");
			if (player == null) {
				player = GameObject.Find("Player");
			}
		}

		if (CONTROLLERS == null) {
			CONTROLLERS = GameObject.Find ("CONTROLLERS");
		}
		_ec = CONTROLLERS.GetComponent<ArcheryEnemyController> ();

		if (location_1 == null) {
			location_1 = GameObject.Find("location (1)").transform;
		}
		if (location_2 == null) {
			location_2 = GameObject.Find("location (2)").transform;
		}
		if (location_3 == null) {
			location_3 = GameObject.Find("location (3)").transform;
		}
		if (location_4 == null) {
			location_4 = GameObject.Find("location (4)").transform;
		}
		if (location_5 == null) {
			location_5 = GameObject.Find("location (5)").transform;
		}
		if (location_6 == null) {
			location_6 = GameObject.Find("location (6)").transform;
		}

		changePlayerLocation (FIRST_POSITION);
		playerLocation = 1;

		game_clock = 0.0f;

		if (wave_break_length < 15.0f) {
			wave_break_timer = 20.0f;
		} 
		else {
			wave_break_timer = wave_break_length;
		}
		wave_counter = 1;
		startNextWave ();

		isPaused = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.P) == true) {
			isPaused = !(isPaused);
		}

		if (Input.GetKeyDown (KeyCode.C) == true ) {
			Cursor.visible = !(Cursor.visible);
		}

		if (isPaused == false) {

			game_clock += Time.deltaTime;

			if (Input.GetKeyDown (KeyCode.Alpha1) == true || Input.GetKeyDown (KeyCode.Keypad1) == true) {
				changePlayerLocation (FIRST_POSITION);
			}
			if (Input.GetKeyDown (KeyCode.Alpha2) == true || Input.GetKeyDown (KeyCode.Keypad2) == true) {
				changePlayerLocation (SECOND_POSITION);
			}
			if (Input.GetKeyDown (KeyCode.Alpha3) == true || Input.GetKeyDown (KeyCode.Keypad3) == true) {
				changePlayerLocation (THIRD_POSITION);
			}
			if (Input.GetKeyDown (KeyCode.Alpha4) == true || Input.GetKeyDown (KeyCode.Keypad4) == true) {
				changePlayerLocation (FOURTH_POSITION);
			}
			if (Input.GetKeyDown (KeyCode.Alpha5) == true || Input.GetKeyDown (KeyCode.Keypad5) == true) {
				changePlayerLocation (FIFTH_POSITION);
			}
			if (Input.GetKeyDown (KeyCode.Alpha6) == true || Input.GetKeyDown (KeyCode.Keypad6) == true) {
				changePlayerLocation (SIXTH_POSITION);
			}
			if ( (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl)) && Input.GetKeyDown(KeyCode.P)){
				UnityEngine.SceneManagement.SceneManager.LoadScene("TheMenu");
			}

			if (_ec.getIsWaveComplete () == true && isBetweenWaves == false) {
				isBetweenWaves = true;
				_ec.clearWave ();
				wave_counter++;
				if (wave_counter > MAX_WAVES) {	/********* VICTORY CONDITION **********  @David*/ 
					/*GAME IS COMPLETE */
					UnityEngine.SceneManagement.SceneManager.LoadScene("TheMenu");
				}
			}

			if (isBetweenWaves == true) {
				wave_break_timer -= Time.deltaTime;

				if (wave_break_timer <= 0.0f) {
					startNextWave ();
				}
			}

		}
	
	}

	public float getTimeElapsed(){
		return game_clock;
	}

	public float getWaveBreakCountdown(){
		return wave_break_timer;
	}

	public int getWaveCounter(){
		return wave_counter;
	}

	public bool getIsPaused(){
		return isPaused;
	}

	public void setIsPaused(){
		isPaused = !(isPaused);
	}

	public bool getIsBetweenWaves(){
		return isBetweenWaves;
	}

	private void startNextWave(){
		isBetweenWaves = false;
		wave_break_timer = wave_break_length;

		switch (wave_counter) {
			case 1:		{_ec.setupForNextWave (numEnemies_wave1);}
				break;
			case 2:		{_ec.setupForNextWave (numEnemies_wave2);}
				break;
			case 3:		{_ec.setupForNextWave (numEnemies_wave3);}
				break;
			case 4:		{_ec.setupForNextWave (numEnemies_wave4);}
				break;
			case 5:		{_ec.setupForNextWave (numEnemies_wave5);}
				break;
			default:	{_ec.setupForNextWave (numEnemies_default);}
				break;
		}


	}//Method -- startNextWave

	public void cyclePlayerLocation(){
		playerLocation++;
		if (playerLocation > NUM_LOCATIONS) {
			playerLocation = 1;
		}
		changePlayerLocation(playerLocation);
	}

	private void changePlayerLocation(int loc){

		switch(loc){
			case 1:		{
				if (location_1 == null) {
					Debug.Log ("location_1 was null when a change in location was attempted.");
					return;
				}
				player.transform.position = Vector3.Lerp (player.transform.position, location_1.position, Time.deltaTime * TELEPORT);
				player.transform.rotation = Quaternion.Lerp (player.transform.rotation, location_1.rotation, Time.deltaTime * TELEPORT);
			}
				break;
			case 2:		{
				if (location_2 == null) {
					Debug.Log ("location_2 was null when a change in location was attempted.");
					return;
				}
				player.transform.position = Vector3.Lerp (player.transform.position, location_2.position, Time.deltaTime * TELEPORT);
				player.transform.rotation = Quaternion.Lerp (player.transform.rotation, location_2.rotation, Time.deltaTime * TELEPORT);
			}
				break;
			case 3:		{
				if (location_3 == null) {
					Debug.Log ("location_3 was null when a change in location was attempted.");
					return;
				}
				player.transform.position = Vector3.Lerp (player.transform.position, location_3.position, Time.deltaTime * TELEPORT);
				player.transform.rotation = Quaternion.Lerp (player.transform.rotation, location_3.rotation, Time.deltaTime * TELEPORT);
			}
				break;
			case 4:		{
				if (location_4 == null) {
					Debug.Log ("location_4 was null when a change in location was attempted.");
					return;
				}
				player.transform.position = Vector3.Lerp (player.transform.position, location_4.position, Time.deltaTime * TELEPORT);
				player.transform.rotation = Quaternion.Lerp (player.transform.rotation, location_4.rotation, Time.deltaTime * TELEPORT);
			}
				break;
			case 5:		{
				if (location_5 == null) {
					Debug.Log ("location_5 was null when a change in location was attempted.");
					return;
				}
				player.transform.position = Vector3.Lerp (player.transform.position, location_5.position, Time.deltaTime * TELEPORT);
				player.transform.rotation = Quaternion.Lerp (player.transform.rotation, location_5.rotation, Time.deltaTime * TELEPORT);
			}
				break;
			case 6:		{
				if (location_6 == null) {
					Debug.Log ("location_6 was null when a change in location was attempted.");
					return;
				}
				player.transform.position = Vector3.Lerp (player.transform.position, location_6.position, Time.deltaTime * TELEPORT);
				player.transform.rotation = Quaternion.Lerp (player.transform.rotation, location_6.rotation, Time.deltaTime * TELEPORT);
			}
				break;
			default:		{
				player.transform.position = Vector3.Lerp (player.transform.position, location_1.position, Time.deltaTime * TELEPORT);
				player.transform.rotation = Quaternion.Lerp (player.transform.rotation, location_1.rotation, Time.deltaTime * TELEPORT);
			}
				break;
		}//switch - loc

		view_player_location = loc;
	}//Method -- changePlayerLocation
		
}//CLASS BRACKET
