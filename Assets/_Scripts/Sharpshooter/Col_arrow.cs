﻿using UnityEngine;
using System.Collections;

/**
 * <summary>
 * Col_arrow manages the state of the arrows in the level "Sharpshooter."
 * </summary>
 */
public class Col_arrow : MonoBehaviour {

	private bool hasMadeContact = false;

	private Rigidbody rb;

	void Start (){
		rb = this.gameObject.GetComponent<Rigidbody> ();
	}

	void OnTriggerEnter(Collider info){

		if (info.GetComponent<Collider>().tag.Equals("Wall")) {
			//print ("COLLISION enter");
			hasMadeContact = true;
			rb.velocity = Vector3.zero;
		}

		if (info.GetComponent<Collider>().tag.Equals("Enemy")) {
			//print ("\"" + info.gameObject.name + "\" has been *hit* (health: "+info.gameObject.GetComponent<Col_enemy>().getHealth()+")");
			hasMadeContact = true;
			rb.velocity = Vector3.zero;
		}

		if (info.GetComponent<Collider>().tag.Equals("Ground")) {
			hasMadeContact = true;
			rb.velocity = Vector3.zero;
		}
	}

	void OnTriggerStay(Collider info){

	}

	void OnTriggerExit(Collider info){
		if (info.GetComponent<Collider>().tag.Equals ("Wall")) {
			//print ("COLLISION exit");
		}
	}

	public bool getHasMadeContact(){
		return hasMadeContact;
	}


}
