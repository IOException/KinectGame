﻿using UnityEngine;
using System.Collections;

/**
 * <summary>
 * Col_enemy manages the state of the enemies in the level "Sharpshooter."
 * </summary>
 */
public class Col_enemy : MonoBehaviour {

	[Tooltip("The starting health for this enemy")]
	public int set_health;

	private int health;
	private int damage = 5;
	private bool isDying;
	private bool isDead;

	//viewing varaibles
	public bool view_isDying;
	public bool view_isDead;

	// Use this for initialization
	void Start () {

		if (set_health < 5 || set_health > 20) {
			health = 10;
		}
		else {
			health = set_health;
		}
		isDying = false;
		isDead = false;
	
	}

	void OnTriggerEnter(Collider info){

		if (info.GetComponent<Collider>().tag.Equals("Arrow")) {

			if (health > 0) {
				health -= damage;
			}

		}
	}

	public bool getIsDying(){
		return isDying;
	}

	public void setIsDead(bool b){
		isDead = b;
	}

	public bool getIsDead(){
		return isDead;
	}

	public int getHealth(){
		return health;
	}

	void Update(){
		if (health <= 0) {
			isDead = true;
		}

		view_isDead = isDead;
		view_isDying = isDying;
	}
}
