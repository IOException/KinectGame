﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HudController : MonoBehaviour {

	//ACESSING OTHER SCRIPTS
	[Tooltip("The GameObject that contains the scripts that control the game")]
	public GameObject CONTROLLERS;
	private ArcheryEnemyController _ec;
	private ArcheryGameController _gc;

	[Tooltip("The UI Text Object that displays the time")]
	public Text t_time;

	[Tooltip("The UI Text Object that displays the wave number")]
	public Text t_wave_number;

	[Tooltip("The UI Text Object that displays the enemies remaining")]
	public Text t_enemies_left;

	[Tooltip("The UI Text Object that displays the pause status of the game")]
	public Text t_paused;

	[Tooltip("The UI Text Object that displays the countdown until the next wave")]
	public Text t_countdown;

	[Tooltip("The UI Slider Object that displays approximately how many enemies remain")]
	public Slider slider;

	//COLOR VARIABLES
	private Color RED = new Color(255, 0, 0);
	//private Color ORANGE = new Color(255, 106, 0);
	//private Color YELLOW = new Color(255, 255, 0); 
	private Color GREEN = new Color(0, 255, 0);
	private Color BLUE = new Color(0, 0, 255);
	private Color PURPLE = new Color(178, 0, 255); 
	//private Color WHITE = new Color(255, 255, 255); 
	//private Color BLACK = new Color(0, 0, 0);


	// Use this for initialization
	void Start () {
		if (CONTROLLERS == null) {
			CONTROLLERS = GameObject.Find ("CONTROLLERS");
		}
		_ec = CONTROLLERS.GetComponent<ArcheryEnemyController> ();
		_gc = CONTROLLERS.GetComponent<ArcheryGameController> ();
	
		t_enemies_left.text = "Enemies left";
	}
	
	// Update is called once per frame
	void Update () {

		updateTimeText ();
		updateWaveNumber ();
		//updateEnemiesLeft ();
		updateCountdownTimer ();

		t_paused.gameObject.SetActive (_gc.getIsPaused());
		t_countdown.gameObject.SetActive (_gc.getIsBetweenWaves());

	}

	private void updateTimeText(){
		int time_s = (int)_gc.getTimeElapsed ();

		int minutes = 0;
		int seconds = time_s;

		while (seconds > 59) {

			seconds -= 60;
			minutes++;
		}

		string timeString = "";
		if (seconds < 10) {
			timeString = minutes + ":0" + seconds;
		} 
		else {
			timeString = minutes + ":" + seconds;
		}	

		//t_time.color = PURPLE;
		t_time.text = "Time Elapsed: " + timeString;

	}//Method -- updateTimeText

	private void updateWaveNumber(){
		//t_wave_number.color = PURPLE;
		t_wave_number.text = "Wave #" + _gc.getWaveCounter ();
	}

	private void updateEnemiesLeft(){
		/*
		int amount = _ec.getEnemiesRemaining();
		if (amount <= 10) {
			t_enemies_left.color = GREEN;
		}
		else {
			t_enemies_left.color = RED;
		}

		if (amount < 0) {
			amount = 0;
		}
		*/
		t_enemies_left.text = "Enemies left";
	}

	private void updateCountdownTimer(){
		int time_s = (int)_gc.getWaveBreakCountdown ();

		if (time_s < 6) {
			t_countdown.color = RED;
		}
		else {
			t_countdown.color = BLUE;
		}

		if (time_s > 0)
			t_countdown.text = "Next wave spawning in " + time_s;
		else
			t_countdown.text = "Next wave spawning NOW!";
	}

	public void initSliderValues(int maxNumEnemies){
		slider.minValue = 0;
		slider.maxValue = maxNumEnemies;
		slider.value = slider.maxValue;

	}

	public void decreaseSliderValue(int decrement){
		slider.value -= decrement;
	}

	public void increaseSliderValue(int increment){
		slider.value += increment;
	}
}
