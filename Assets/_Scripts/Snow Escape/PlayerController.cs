﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public GameObject Player;
    public GameObject PlayerModel;
    public GameObject Torse;
    public GameObject Hip;
    public GameObject LeftHand;
    public GameObject RightHand;
    public GameObject LeftElbow;
    public GameObject RightElbow;
    CharacterController controller;
    public float movementSpeed;
    public float rotateSpeed;
    public float gravity = 10.0F;
    private Vector3 moveDirection = Vector3.zero;

    float startTime;
    bool start;
    Animator playerAnimator;
    // Use this for initialization
    void Start() {
        controller = Player.GetComponent<CharacterController>();
        start = false;
        playerAnimator = PlayerModel.GetComponent<Animator>();
        //left.wrapMode = WrapMode.ClampForever;
        //right.wrapMode = WrapMode.ClampForever;
    }

    // Update is called once per frame
    void Update()
    {
        float lateralSpeed =new Vector2(Player.GetComponent<Rigidbody>().velocity.x, Player.GetComponent<Rigidbody>().velocity.z).magnitude;
        //Assumes forward is oriented correctly
        Player.GetComponent<Rigidbody>().velocity = new Vector3(Player.transform.forward.x * lateralSpeed, Player.GetComponent<Rigidbody>().velocity.y, Player.transform.forward.z * lateralSpeed);
      
        //Debug.Log(Player.GetComponent<Rigidbody>().velocity);
        if (LeftHand.transform.position.y > LeftElbow.transform.position.y && RightHand.transform.position.y > RightElbow.transform.position.y)
        {
            startTime = Time.time;
            start = true;
            playerAnimator.SetBool("Raise", true);
            Debug.Log("start");
        }
        else
        {
            playerAnimator.SetBool("Raise", false);
        }
        if (LeftHand.transform.position.z < Hip.transform.position.z && RightHand.transform.position.z < Hip.transform.position.z && start == true)
        {
            Player.GetComponent<Rigidbody>().AddForce(Player.transform.forward* movementSpeed*(1-2*Mathf.Abs(Player.GetComponent<Rigidbody>().velocity.x)/20));
            start = false;
            Debug.Log("end");
            playerAnimator.Play("Down");
        }
        Debug.Log(Torse.transform.position.x - Hip.transform.position.x);

        if(Torse.transform.position.x - Hip.transform.position.x > 0.03)
        {
            playerAnimator.SetBool("Right", true);
        }else if(Torse.transform.position.x - Hip.transform.position.x < -0.03)
        {
            playerAnimator.SetBool("Left", true);
        }
        else
        {
            playerAnimator.SetBool("Left", false);
            playerAnimator.SetBool("Right", false);
            playerAnimator.SetBool("Back", true);

        }

        {
            //moveDirection = Player.transform.right * rotateSpeed * (Torse.transform.position.x - Hip.transform.position.x);
            //Player.GetComponent<Rigidbody>().AddForce(moveDirection*Player.GetComponent<Rigidbody>().velocity.x);
            Player.transform.Rotate(new Vector3(0,1,0) * (Torse.transform.position.x - Hip.transform.position.x)* Time.deltaTime* rotateSpeed);
        }

        if (Player.transform.position.x < 700)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
            
        
    }
}
