﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;

public class PlayVideo : MonoBehaviour {

	public GameObject screen;
	public MovieTexture movTexture;

	// Use this for initialization
	void Start () {
		screen.GetComponent<Renderer> ().material.mainTexture = movTexture;
		movTexture.Play();
	}	

}
