﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextControl_appear : MonoBehaviour {
	//The path to the directory with all the text files
	private const string dir = "Assets\\Text_Files\\";

	//Variables to access the other script
	public Canvas canvas;
	private b_skipCutscene_9 _bSC;		//The Default button script to use

	//The actual Text Object
	public Text storyText;

	//Text-Related Variables
	//int index = 0;
	string x;
	string caption;
	//char[] a;
	int currentScene;

	//Timer-Related Variables
	//private float text_timer;
	//public float timer_increment;
	//private float next_step;
	bool isOver;

	//Color variables (standard)
	private Color RED = new Color(255, 0, 0);
	private Color BLUE = new Color(0, 0, 255);
	private Color WHITE = new Color(255, 255, 255);

	//viewing variable
	//public float view_text_timer;
	//public float view_next_step;

	// Use this for initialization
	void Start () {
		_bSC = canvas.GetComponent<b_skipCutscene_9> ();

		storyText.text = "";
		storyText.color = WHITE;
		/*
		text_timer = 0.0f;
		if (timer_increment < 0) {
			timer_increment = 0.1f;
		}
		next_step = text_timer + timer_increment;

		view_text_timer = text_timer;
		view_next_step = next_step;
		*/
		caption = System.IO.File.ReadAllText(dir + "default.txt");
		//a = caption.ToCharArray ();

		//x = "A long time ago, in a galaxy far, far away...";
		//a = x.ToCharArray ();

		currentScene = _bSC.getActiveScene();

		changeSceneSettings (currentScene);
		isOver = false;

	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.R) == true) {
			storyText.color = RED;
		}
		if (Input.GetKeyDown (KeyCode.B) == true) {
			storyText.color = BLUE;
		}
		if (Input.GetKeyDown (KeyCode.W) == true) {
			storyText.color = WHITE;
		}
		if (Input.GetKeyDown (KeyCode.Escape) == true) {
			Application.Quit ();
		}

		//text_timer += Time.deltaTime;
		//view_text_timer = text_timer;
		
		if (isOver == true){
			Application.Quit ();
		}

		bool testChange = checkSceneChanged ();
		if ( testChange == true) {
			changeSceneSettings (currentScene);
		}

		

		/*
		if (text_timer > next_step) {
			if (index < a.Length) {
				storyText.text += a [index];
				index++;
			} else {
				storyText.text += "\n";
				index = 0;
			}
			next_step = text_timer + timer_increment;

			view_next_step = next_step;

		}
		*/
	}

	private void resetText(){
		storyText.text = "";
		//index = 0;
	}

	private bool checkSceneChanged(){
		if (currentScene == _bSC.getActiveScene())
			return false;
		else {
			currentScene = _bSC.getActiveScene();
			return true;
		}
	}

	private void changeSceneSettings(int currentScene){

		switch (currentScene) {
		case 1:{
				//storyText.color = WHITE;
				changeSceneText("s1_text.txt");
			}
			break;
		case 2:{
				//storyText.color = CYAN;
				changeSceneText("s2_text.txt");
			}
			break;
		case 3:{
				//storyText.color = YELLOW;
				changeSceneText("s3_text.txt");
			}
			break;
		case 4:{
				//storyText.color = GREEN;
				changeSceneText("s4_text.txt");
			}
			break;
		case 5:{
				//storyText.color = BLUE;
				changeSceneText("s5_text.txt");
			}
			break;
		case 6:{
				//storyText.color = PURPLE;
				changeSceneText("s6_text.txt");
			}
			break;
		case 7:{
				//storyText.color = PINK;
				changeSceneText("s7_text.txt");
			}
			break;
		case 8:{
				//storyText.color = BLACK;
				changeSceneText("s8_text.txt");
			}
			break;
		case 9:{
				//storyText.color = RED;
				changeSceneText("s9_text.txt");
			}
			break;
		default:{
				//storyText.color = YELLOW;
				//changeSceneText("default.txt");
				isOver = true;
			}
			break;
		}//switch

	}//Method -- changeSceneSettings

	private void changeSceneText(string file){

		if (file.EndsWith (".txt") == false) {
			file += ".txt";
		}

		try{
			caption = System.IO.File.ReadAllText(dir + file);
		}
		catch(MissingReferenceException){
			caption = "... ... ...";
		}

		storyText.text = caption;
	}

}//CLASS BRACKET


