﻿using UnityEngine;
using System.Collections;

public class TitleOverlayStarter : MonoBehaviour {

	//Variables to access the other script
	public GameObject Animation_Controller;
	private AnimationStarter _AS;

	public Canvas title_overlay;
	private bool control;
	bool isCameraSet;

	// Use this for initialization
	void Start () {
		if (Animation_Controller == null) {
			Animation_Controller = GameObject.Find ("Animation_Controller");
		}
		_AS = Animation_Controller.GetComponent<AnimationStarter> ();

		control = false;
		isCameraSet = false;
	}
	
	// Update is called once per frame
	void Update () {

		isCameraSet = _AS.getHasCameraStopped ();

		if (isCameraSet == true && control == false) {
			title_overlay.gameObject.SetActive (true);
		}
	
	}
}
