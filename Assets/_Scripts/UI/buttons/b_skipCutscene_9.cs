﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class b_skipCutscene_9 : MonoBehaviour {

	public RawImage scene_1;
	public RawImage scene_2;
	public RawImage scene_3;
	public RawImage scene_4;
	public RawImage scene_5;
	public RawImage scene_6;
	public RawImage scene_7;
	public RawImage scene_8;
	public RawImage scene_9;

	public AudioClip audio_1;
	public AudioClip audio_2;
	public AudioClip audio_3;
	public AudioClip audio_4;
	public AudioClip audio_5;
	public AudioClip audio_6;
	public AudioClip audio_7;
	public AudioClip audio_8;
	public AudioClip audio_9;

	public AudioSource sceneAudio;

	private int current = 1;

	public int getActiveScene(){
		return current;
	}

	// Use this for initialization
	void Start () {
		current = 1;
		scene_1.gameObject.SetActive (true);
		scene_2.gameObject.SetActive (false);
		scene_3.gameObject.SetActive (false);
		scene_4.gameObject.SetActive (false);
		scene_5.gameObject.SetActive (false);
		scene_6.gameObject.SetActive (false);
		scene_7.gameObject.SetActive (false);
		scene_8.gameObject.SetActive (false);
		scene_9.gameObject.SetActive (false);

		sceneAudio.PlayOneShot(audio_1);
	}
	
	public void OnClicked(){
		current++;
		changeScene (current);
	}

	private void changeScene(int active){

		switch (active){
			case 1:{
				scene_1.gameObject.SetActive (true);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
				scene_8.gameObject.SetActive (false);
				scene_9.gameObject.SetActive (false);
			}
				break;
			case 2:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (true);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
				scene_8.gameObject.SetActive (false);
				scene_9.gameObject.SetActive (false);

				sceneAudio.Stop ();
				sceneAudio.PlayOneShot(audio_2);
			}
				break;
			case 3:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (true);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
				scene_8.gameObject.SetActive (false);
				scene_9.gameObject.SetActive (false);

				sceneAudio.Stop ();
				sceneAudio.PlayOneShot(audio_3);
			}
				break;
			case 4:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (true);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
				scene_8.gameObject.SetActive (false);
				scene_9.gameObject.SetActive (false);

				sceneAudio.Stop ();
				sceneAudio.PlayOneShot(audio_4);
			}
				break;
			case 5:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (true);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
				scene_8.gameObject.SetActive (false);
				scene_9.gameObject.SetActive (false);

				sceneAudio.Stop ();
				sceneAudio.PlayOneShot(audio_5);
			}
				break;
			case 6:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (true);
				scene_7.gameObject.SetActive (false);
				scene_8.gameObject.SetActive (false);
				scene_9.gameObject.SetActive (false);

				sceneAudio.Stop ();
				sceneAudio.PlayOneShot(audio_6);
			}
				break;
			case 7:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (true);
				scene_8.gameObject.SetActive (false);
				scene_9.gameObject.SetActive (false);

				sceneAudio.Stop ();
				sceneAudio.PlayOneShot(audio_7);
			}
				break;
			case 8:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
				scene_8.gameObject.SetActive (true);
				scene_9.gameObject.SetActive (false);

				sceneAudio.Stop ();
				sceneAudio.PlayOneShot(audio_8);
			}
				break;
			case 9:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
				scene_8.gameObject.SetActive (false);
				scene_9.gameObject.SetActive (true);

				sceneAudio.Stop ();
				sceneAudio.PlayOneShot(audio_9);
			}
				break;
			default:{
				/*Currently, it just sets all inactive.  In the game, the default case will
				be used to bring the player to the next warm-up, game, or cool down*/
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
				scene_8.gameObject.SetActive (false);
				scene_9.gameObject.SetActive (false);

				sceneAudio.Stop ();

				//Load the next game
				SceneManager.LoadScene("KickGame");

			}

				break;
		}//switch

	}//Method Bracket -- changeScene(...)

}//CLASS BRACKET


























