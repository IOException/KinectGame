﻿using UnityEngine;
using System.Collections;

public class GetKicked : MonoBehaviour
{
    public bool RunLegs;
    public float kickY;
    public float kickZ;
    public float tumble;
    
    LegsDataCollection dataCollect;
    
    Animator anim;
    bool getKicked;
    GameObject[] foots;
    GameObject gameController;

    // Use this for initialization
    void Start()
    {
        if (foots == null)
            foots = GameObject.FindGameObjectsWithTag("Foot");
        anim = GetComponent<Animator>();
        gameController = GameObject.FindGameObjectWithTag("GameController");
        dataCollect = gameController.GetComponent<LegsDataCollection>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Mathf.Abs(foots[0].transform.position.y - foots[1].transform.position.y));
    }
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Foot" && getKicked != true && Mathf.Abs(foots[0].transform.position.y - foots[1].transform.position.y) > 0.1)
        {

            anim.SetBool("getKicked", true);
            GetComponent<Rigidbody>().AddForce(0, kickY, kickZ);
            GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * tumble;
            GetComponent<Rigidbody>().useGravity = true;
            getKicked = true;
            gameController.GetComponent<kickGameController>().add();
        }

    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Boundary")
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "DataCollectionBoundary")
        {    // Debug.Log("GetKicked Works ");
            RunLegs = true;
            dataCollect.startCollect = true; 
        }


       if (other.tag == "KillBoundary")

        {
            gameController.GetComponent<kickGameController>().hp -= 1;

        }
    }
}