﻿using UnityEngine;
using System.Collections;

public class kickGameController : MonoBehaviour {

    public float spawnRange;
    public Vector3 spawnPosition;
    Quaternion spawnRotation;
    public float waitTime;
    public GameObject enemy;
    public UnityEngine.UI.Text scoreText;
    public UnityEngine.UI.Text timerText;
    public UnityEngine.UI.Text gameOverText;
    public GameObject EndPanel;
    public GameObject StartPanel;
    float time;
    int score;
    int timer;
    int startTime;
    bool start;

    public int hp;
    //public GameObject healthUI;
	// Use this for initialization
	void Start () {
        hp = 5;
        start = false;
        score = 0;
        timer = 45;
        spawnRotation = Quaternion.identity;
        spawnRotation.eulerAngles = new Vector3(0, -180, 0);
        chooseStanding();
    }

	void StartGame()
    {
        startTime = (int)Time.time;
        start = true;
        
    }

	// Update is called once per frame
	void Update () {
        //healthUI.GetComponent<HealthUIController>().hp = hp;

        if (start == true)
        {
            timer = (int)(Time.time-startTime);
            //timerText.text = timer.ToString();
            scoreText.text = score.ToString();
        }
        if (hp == 0)
        {
          //  gameOver();
        }
        if (Time.time > time && start == true)
        {
            spawnPosition = new Vector3(Random.Range(-spawnRange, spawnRange), spawnPosition.y, spawnPosition.z);
            Instantiate(enemy, spawnPosition, spawnRotation);
            time = Time.time + waitTime;
        }
	}

    public void add()
    {
        score++;
    }

    void gameOver()
    {
        start = false;
        EndPanel.SetActive(true);
        gameOverText.text = "Game Over \n Your Score is: " + score;
    }

    public void BacktoMap()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void chooseStanding()
    {
        spawnRange = 1;
        StartPanel.SetActive(false);
        StartGame();
    }
    public void chooseSitting()
    {
        spawnRange = 0;
        StartPanel.SetActive(false);
        StartGame();
    }
}
